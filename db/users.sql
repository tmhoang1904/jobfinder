-- phpMyAdmin SQL Dump
-- version 3.5.8.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 22, 2016 at 09:33 AM
-- Server version: 5.5.31
-- PHP Version: 5.3.29

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `quickjob_v2`
--

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(100) CHARACTER SET latin1 NOT NULL,
  `user_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `img_url` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `phone_number` varchar(50) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `experience` text CHARACTER SET latin1,
  `email` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `address` varchar(300) CHARACTER SET latin1 DEFAULT NULL,
  `password` varchar(45) CHARACTER SET latin1 NOT NULL,
  `description` varchar(300) CHARACTER SET latin1 DEFAULT NULL,
  `skill_desc` varchar(500) CHARACTER SET latin1 DEFAULT NULL,
  `delete_flag` tinyint(4) DEFAULT '0',
  `noti_token` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=92 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `user_id`, `user_name`, `img_url`, `phone_number`, `experience`, `email`, `address`, `password`, `description`, `skill_desc`, `delete_flag`, `noti_token`) VALUES
(1, 'admin', 'admin', 'http://xinh.live/media/jobfinder/02b0b2b7-ed68-4627-9933-6d61f46e3b5f.png', '01929300', NULL, 'admin@gmail.com', 'admin Home', 'admin', NULL, NULL, 0, NULL),
(2, 'tuanvu123', 'tuanvu', NULL, '09999999', NULL, 'ghj@gmail.com', '', '123456789', 'Hjkkkk', 'Hjkkkk', 0, NULL),
(3, 'tuanvu9999', 'tuanvu', NULL, '09999999', NULL, 'ghj@gmail.com', '', '123456789', 'Hjkkkk', 'Hjkkkk', 0, NULL),
(4, 'tuanvu1234565', 'tuanvu', NULL, '200080555685', NULL, 'fhb@gmail.com', 'ha Noo', '123456', 'Hiiiiiiiiii', 'Heeeeeee', 0, NULL),
(8, 'vutuan ', 'tuanvuiiiii', 'http://xinh.live/media/jobfinder/3d8a8f65-7704-4301-a4b1-1d1f2a7ff435.png', '111111111111111', NULL, 'Gmail@yahoo.comiiiiiiiii', '', '123456789', NULL, 'Heeeeeee I is weiiiii', 0, NULL),
(9, 'hhhhhh', 'gggggg', NULL, '55555555', NULL, 'hhhhhh@gmail.com', 'jjjjjjj', '123456789', ':::::::::', '::::::', 0, NULL),
(10, 'adtest ', 'tester', NULL, '28564588', NULL, 'gb@bn.com', 'hn', '123456789', 'Hhhh', 'Jjjjjjj', 0, NULL),
(11, 'adtest 1', 'tester', NULL, '28564588', NULL, 'gb@bn.com', 'hn', '123456789', ' ', 'Jjjjjjj', 0, NULL),
(12, 'adtest2', 'test', NULL, '12358', NULL, 'fjjgh@gmail.com', '123456789', '123456789', 'Hhhhhh', 'Hhhhh', 0, NULL),
(13, 'long5555', 'Long John', NULL, '6473336666', NULL, 'long5555@yahoo.com.vn', '123 Yonge st, Toronto, ON, M6H3B6', 'Long1234', 'abc abc abcabc abc abc abc abc abc abc abc abc abc abc abc \nabc abc abc abc abc abc abc abc abc abc abc abc \nabc abc abc abc abc abc abc abc abc abc abc abc \n', 'abc abc abc abc abc abc abc abc abc abc abc abc \nabc abc abc abc abc abc abc abc abc abc abc abc \nabc abc abc abc abc abc abc abc abc abc abc abc ', 0, NULL),
(18, 'newacc', 'tuanvu', NULL, '0989888888', NULL, 'zxc@vb.com', 'ha noi', '123456789', 'K', 'K', 0, NULL),
(19, 'meothqntai', 'Mèo Th?n Tài', NULL, '0934590969', NULL, 'trungbt.lmh@gmail.com', NULL, '123456', NULL, NULL, 0, NULL),
(20, 'trungbt', 'Tu?n Trung', NULL, '0934590969', NULL, 'trungbt.lmh@gmail.com', NULL, '1234', NULL, NULL, 0, NULL),
(21, 'acc1234', 'Mike', 'http://xinh.live/media/jobfinder/b07a51ab-4306-4759-960b-823e3d5f6545.png', '', NULL, 'tuanvu@gmail.com', '', '123456', NULL, '', 0, NULL),
(22, 'john1', 'John Long', NULL, '6475559999', NULL, 'johnlong@longlong.com', NULL, 'Long1234', NULL, NULL, 0, NULL),
(23, 'fixgirl', 'xxxxxxxx', NULL, '+8499999999', NULL, 'bvdfgb@the.com', NULL, '123456', NULL, NULL, 0, NULL),
(24, 'fix', 'xxxxxx', NULL, '09888888', NULL, 'google@gmail.com', NULL, '123456789', NULL, NULL, 0, NULL),
(56, 'tttttt', 'tuan but', NULL, '123456789', NULL, 'hhh@gmail.com', '', '123456', '', '', 0, NULL),
(57, 'theone', 'tuanvu ', NULL, '1080808000', NULL, 'email@gmail.com', NULL, '123456789', NULL, NULL, 0, NULL),
(58, 'theeeeee', 'tuan Minh ', NULL, '0208080008', NULL, 'g@mail.com', NULL, '123456', NULL, NULL, 0, NULL),
(59, 'quickjob001', 'quickjob001', 'http://xinh.live/media/jobfinder/cc97e0be-a8dc-4399-ae9d-89152b8c42aa.png', '0208080005', NULL, 'g@mail.com', '', '123456', NULL, NULL, 0, NULL),
(60, 'quickjob002', 'john ben', '', '0208080008', NULL, 'g@mail.com', '', '123456', NULL, NULL, 0, NULL),
(61, 'quickjobb003', 'quickjob003', NULL, '0208080008', NULL, 'g@mail.com', NULL, '123456', NULL, NULL, 0, NULL),
(62, 'quickjob004', 'quickjob004', NULL, '0208080008', NULL, 'g@mail.com', NULL, '123456', NULL, NULL, 0, NULL),
(63, 'quickjob005', 'quickjob005', NULL, '0208080008', NULL, 'g@mail.com', NULL, '123456', NULL, NULL, 0, NULL),
(64, 'quickjob006', 'quickjob006', NULL, '0208080008', NULL, 'g@mail.com', NULL, '123456', NULL, NULL, 0, NULL),
(65, 'quickjob007', 'quickjob007', NULL, '0208080008', NULL, 'g@mail.com', NULL, '123456', NULL, NULL, 0, NULL),
(66, 'quickjob008', 'quickjob008', NULL, '0208080008', NULL, 'g@mail.com', NULL, '123456', NULL, NULL, 0, NULL),
(67, 'quickjob009', 'quickjob009', NULL, '0208080008', NULL, 'g@mail.com', NULL, '123456', NULL, NULL, 0, NULL),
(68, 'quickjob010', 'quickjob010', NULL, '0208080008', NULL, 'g@mail.com', NULL, '123456', NULL, NULL, 0, NULL),
(69, 'quickjob', 'tuan vu', NULL, '0989848333', NULL, 'gicungdc@gmail.com', NULL, '123456', NULL, NULL, 0, NULL),
(83, '3032059036', 'tuananha2', 'https://twitter.com/tuananha2/profile_image?size=original', '0898989898', NULL, '2457@gmail.com', 'New York City ', '4f51727c-7a20-4033-8065-50fa14a320af', 'I''m so at a news report on that day and a night of sleep last in a statement that it is not the best thing about it this time year and the I have no idea what the I don''t have a nice dream of you ', 'The fact best of thing that could I be on that note to investors and I don''t have think of it and it is not a fan since day of school for a long few weeks and I''m months of my favorite friends with are all ', 0, NULL),
(86, '1755693211353881', 'Bùi Tuấn Trung', 'http://graph.facebook.com/1755693211353881/picture?type=large&redirect=true&width=500&height=500', '', '', '', '', 'd3f3131e-38ef-4ff2-b7b3-176e0460d8a8', '', '', 0, NULL),
(89, '1657605701228705', 'Tuấn Vũ', 'http://graph.facebook.com/1657605701228705/picture?type=square&width=500&height=500', '2088855888', NULL, 'Gjbb@hhhhhh.com', 'The I''m not so a going on a with me my phone life ', '82f2082b-97c4-49f5-a73e-8448e896125f', 'The fact I best friend is in my fact is a not a fan and since then has to go back in and out of bed a ', 'Tijuana to the fact is I don''t think I have to a YouTube playlist is the most beautiful girl I in the first half of the year before I that the only thing is ', 0, NULL),
(90, '597370567101000', 'Tuấn Linh', 'http://graph.facebook.com/597370567101000/picture?type=square&width=500&height=500', '', '', '', '', '122679e9-7676-443d-b54c-2169753d94f5', '', '', 0, NULL),
(91, 'hh', 'bb', '', '5855666', NULL, 'gvvbb@gmail.com', NULL, 'dfhhjj', NULL, NULL, 0, NULL);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

-- phpMyAdmin SQL Dump
-- version 3.5.8.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 22, 2016 at 09:32 AM
-- Server version: 5.5.31
-- PHP Version: 5.3.29

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `quickjob_v2`
--

-- --------------------------------------------------------

--
-- Table structure for table `skill_set`
--

CREATE TABLE IF NOT EXISTS `skill_set` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `skill` varchar(100) DEFAULT NULL,
  `delete_flag` tinyint(1) DEFAULT '0',
  `icon` varchar(300) NOT NULL,
  `noti_token` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=24 ;

--
-- Dumping data for table `skill_set`
--

INSERT INTO `skill_set` (`id`, `skill`, `delete_flag`, `icon`, `noti_token`) VALUES
(1, 'Architecture and Engineering Occupations', 0, 'https://cdn3.iconfinder.com/data/icons/map-markers-1/512/repair_service-128.png', 'fE9uAO8yWYw:APA91bHfs0oz3jE9uLPXBTPxQJcNqx5VfQNFlc9YWYCs36TmrKhp7_Ezbu5gJnxoLdzDtfUjDNrYbNTrWjYYH3u0s4EBIiLpzIc5fQ6LQcNVUvEpkJyfpBKGjGiMu3f0Y5ynzmN3AHGj,dZ06ErPFSYo:APA91bF9Z00kmsrF-zuydg1fkrSYALKy9mwi3rr3ZHjwjfDNr3CLpsVfRVpslDfqdY6dXJ8jMLJLiffyAXXnlUVANn_I8nd1TAf9RBVMU389okzIg4m6J3LAABLINYW7CrAyawj81jnU,cPV-gIBkRBI:APA91bFzIH8F4-5gkqE5Br1b-7KbeTObmcLHSIZRPQIch5gyuvdWfM3shUc4uWkVnTLB0_HB1c0jotIscLgIDiK1I5gSc4hvvv9jFarRbYHj9gsLLJKNCtMEY_Gz8aNraxfmwczwq_UO,f2HvwsHUqKQ:APA91bFjlRlKXLGEKm7L0erW4YvwySiBmN43bJQXwhipe8lBVYtEykF8iw88BJk7nmxXUohINmGvuNr0DlgaPAtIvUJCgNURBNKZzZmxl8zJNkO9wEEDEqp-2cKfNf1AWonIw-l5AXv0,dXY-RkA3DlU:APA91bFA2cyoVOZIWsl_JC281g9xAr5I5zMevYlkw8jT5A3SlL-cH6BM202NTwxSye4XXh25L_eNEncb01Vxm1aHv2LRLiw1fl6II-KhbFdXU_Yv25q7pHDRiB8XE3LBuVVDWsBo5VCa'),
(2, 'Arts, Design, Entertainment, Sports, and Media Occupations', 0, 'https://cdn3.iconfinder.com/data/icons/map-markers-1/512/phone_connection-128.png', 'fE9uAO8yWYw:APA91bHfs0oz3jE9uLPXBTPxQJcNqx5VfQNFlc9YWYCs36TmrKhp7_Ezbu5gJnxoLdzDtfUjDNrYbNTrWjYYH3u0s4EBIiLpzIc5fQ6LQcNVUvEpkJyfpBKGjGiMu3f0Y5ynzmN3AHGj,dZ06ErPFSYo:APA91bF9Z00kmsrF-zuydg1fkrSYALKy9mwi3rr3ZHjwjfDNr3CLpsVfRVpslDfqdY6dXJ8jMLJLiffyAXXnlUVANn_I8nd1TAf9RBVMU389okzIg4m6J3LAABLINYW7CrAyawj81jnU,d3bAQuICG3c:APA91bGLB6ttfwVBkeTOyEW_En5lFknrJx833o_s2VnFMG_g178OudGkNx5lj-6GLtvO-keW5ZtgJYMf3RdsNSKnua2pxvhsFuvpGu3ZYFEWBYboO85Kt7kzScDC2mq3StYy2rsmfs_G,cPV-gIBkRBI:APA91bFzIH8F4-5gkqE5Br1b-7KbeTObmcLHSIZRPQIch5gyuvdWfM3shUc4uWkVnTLB0_HB1c0jotIscLgIDiK1I5gSc4hvvv9jFarRbYHj9gsLLJKNCtMEY_Gz8aNraxfmwczwq_UO,f2HvwsHUqKQ:APA91bFjlRlKXLGEKm7L0erW4YvwySiBmN43bJQXwhipe8lBVYtEykF8iw88BJk7nmxXUohINmGvuNr0DlgaPAtIvUJCgNURBNKZzZmxl8zJNkO9wEEDEqp-2cKfNf1AWonIw-l5AXv0,dXY-RkA3DlU:APA91bFA2cyoVOZIWsl_JC281g9xAr5I5zMevYlkw8jT5A3SlL-cH6BM202NTwxSye4XXh25L_eNEncb01Vxm1aHv2LRLiw1fl6II-KhbFdXU_Yv25q7pHDRiB8XE3LBuVVDWsBo5VCa'),
(3, 'Building and Grounds Cleaning and Maintenance Occupations', 0, 'https://cdn3.iconfinder.com/data/icons/map-markers-1/512/lorry-128.png', 'fE9uAO8yWYw:APA91bHfs0oz3jE9uLPXBTPxQJcNqx5VfQNFlc9YWYCs36TmrKhp7_Ezbu5gJnxoLdzDtfUjDNrYbNTrWjYYH3u0s4EBIiLpzIc5fQ6LQcNVUvEpkJyfpBKGjGiMu3f0Y5ynzmN3AHGj,dZ06ErPFSYo:APA91bF9Z00kmsrF-zuydg1fkrSYALKy9mwi3rr3ZHjwjfDNr3CLpsVfRVpslDfqdY6dXJ8jMLJLiffyAXXnlUVANn_I8nd1TAf9RBVMU389okzIg4m6J3LAABLINYW7CrAyawj81jnU,cPV-gIBkRBI:APA91bFzIH8F4-5gkqE5Br1b-7KbeTObmcLHSIZRPQIch5gyuvdWfM3shUc4uWkVnTLB0_HB1c0jotIscLgIDiK1I5gSc4hvvv9jFarRbYHj9gsLLJKNCtMEY_Gz8aNraxfmwczwq_UO,f2HvwsHUqKQ:APA91bFjlRlKXLGEKm7L0erW4YvwySiBmN43bJQXwhipe8lBVYtEykF8iw88BJk7nmxXUohINmGvuNr0DlgaPAtIvUJCgNURBNKZzZmxl8zJNkO9wEEDEqp-2cKfNf1AWonIw-l5AXv0,dXY-RkA3DlU:APA91bFA2cyoVOZIWsl_JC281g9xAr5I5zMevYlkw8jT5A3SlL-cH6BM202NTwxSye4XXh25L_eNEncb01Vxm1aHv2LRLiw1fl6II-KhbFdXU_Yv25q7pHDRiB8XE3LBuVVDWsBo5VCa'),
(4, 'Business and Financial Operations Occupations', 0, '', NULL),
(5, 'Community and Social Services Occupations', 0, '', 'fE9uAO8yWYw:APA91bHfs0oz3jE9uLPXBTPxQJcNqx5VfQNFlc9YWYCs36TmrKhp7_Ezbu5gJnxoLdzDtfUjDNrYbNTrWjYYH3u0s4EBIiLpzIc5fQ6LQcNVUvEpkJyfpBKGjGiMu3f0Y5ynzmN3AHGj,dZ06ErPFSYo:APA91bF9Z00kmsrF-zuydg1fkrSYALKy9mwi3rr3ZHjwjfDNr3CLpsVfRVpslDfqdY6dXJ8jMLJLiffyAXXnlUVANn_I8nd1TAf9RBVMU389okzIg4m6J3LAABLINYW7CrAyawj81jnU,d3bAQuICG3c:APA91bGLB6ttfwVBkeTOyEW_En5lFknrJx833o_s2VnFMG_g178OudGkNx5lj-6GLtvO-keW5ZtgJYMf3RdsNSKnua2pxvhsFuvpGu3ZYFEWBYboO85Kt7kzScDC2mq3StYy2rsmfs_G,cPV-gIBkRBI:APA91bFzIH8F4-5gkqE5Br1b-7KbeTObmcLHSIZRPQIch5gyuvdWfM3shUc4uWkVnTLB0_HB1c0jotIscLgIDiK1I5gSc4hvvv9jFarRbYHj9gsLLJKNCtMEY_Gz8aNraxfmwczwq_UO,f2HvwsHUqKQ:APA91bFjlRlKXLGEKm7L0erW4YvwySiBmN43bJQXwhipe8lBVYtEykF8iw88BJk7nmxXUohINmGvuNr0DlgaPAtIvUJCgNURBNKZzZmxl8zJNkO9wEEDEqp-2cKfNf1AWonIw-l5AXv0,dXY-RkA3DlU:APA91bFA2cyoVOZIWsl_JC281g9xAr5I5zMevYlkw8jT5A3SlL-cH6BM202NTwxSye4XXh25L_eNEncb01Vxm1aHv2LRLiw1fl6II-KhbFdXU_Yv25q7pHDRiB8XE3LBuVVDWsBo5VCa'),
(6, 'Computer and Mathematical Occupations', 0, '', 'fE9uAO8yWYw:APA91bHfs0oz3jE9uLPXBTPxQJcNqx5VfQNFlc9YWYCs36TmrKhp7_Ezbu5gJnxoLdzDtfUjDNrYbNTrWjYYH3u0s4EBIiLpzIc5fQ6LQcNVUvEpkJyfpBKGjGiMu3f0Y5ynzmN3AHGj,dZ06ErPFSYo:APA91bF9Z00kmsrF-zuydg1fkrSYALKy9mwi3rr3ZHjwjfDNr3CLpsVfRVpslDfqdY6dXJ8jMLJLiffyAXXnlUVANn_I8nd1TAf9RBVMU389okzIg4m6J3LAABLINYW7CrAyawj81jnU,cPV-gIBkRBI:APA91bFzIH8F4-5gkqE5Br1b-7KbeTObmcLHSIZRPQIch5gyuvdWfM3shUc4uWkVnTLB0_HB1c0jotIscLgIDiK1I5gSc4hvvv9jFarRbYHj9gsLLJKNCtMEY_Gz8aNraxfmwczwq_UO,f2HvwsHUqKQ:APA91bFjlRlKXLGEKm7L0erW4YvwySiBmN43bJQXwhipe8lBVYtEykF8iw88BJk7nmxXUohINmGvuNr0DlgaPAtIvUJCgNURBNKZzZmxl8zJNkO9wEEDEqp-2cKfNf1AWonIw-l5AXv0,dXY-RkA3DlU:APA91bFA2cyoVOZIWsl_JC281g9xAr5I5zMevYlkw8jT5A3SlL-cH6BM202NTwxSye4XXh25L_eNEncb01Vxm1aHv2LRLiw1fl6II-KhbFdXU_Yv25q7pHDRiB8XE3LBuVVDWsBo5VCa'),
(7, 'Construction and Extraction Occupations', 0, '', NULL),
(8, 'Education, Training, and Library Occupations', 0, '', NULL),
(9, 'Farming, Fishing, and Forestry Occupations', 0, '', NULL),
(10, 'Food Preparation and Serving Related Occupations', 0, '', NULL),
(11, 'Healthcare Practitioners and Technical Occupations', 0, '', NULL),
(12, 'Healthcare Support Occupations', 0, '', NULL),
(13, 'Installation, Maintenance, and Repair Occupations', 0, '', NULL),
(14, 'Legal Occupations', 0, '', NULL),
(15, 'Life, Physical, and Social Science Occupations', 0, '', NULL),
(16, 'Management Occupations\n', 0, '', NULL),
(17, 'Military Specific Occupations\n', 0, '', NULL),
(18, 'Office and Administrative Support Occupations', 0, '', NULL),
(19, 'Personal Care and Service Occupations', 0, '', NULL),
(20, 'Production Occupations', 0, '', NULL),
(21, 'Protective Service Occupations', 0, '', 'fE9uAO8yWYw:APA91bHfs0oz3jE9uLPXBTPxQJcNqx5VfQNFlc9YWYCs36TmrKhp7_Ezbu5gJnxoLdzDtfUjDNrYbNTrWjYYH3u0s4EBIiLpzIc5fQ6LQcNVUvEpkJyfpBKGjGiMu3f0Y5ynzmN3AHGj,dZ06ErPFSYo:APA91bF9Z00kmsrF-zuydg1fkrSYALKy9mwi3rr3ZHjwjfDNr3CLpsVfRVpslDfqdY6dXJ8jMLJLiffyAXXnlUVANn_I8nd1TAf9RBVMU389okzIg4m6J3LAABLINYW7CrAyawj81jnU,cPV-gIBkRBI:APA91bFzIH8F4-5gkqE5Br1b-7KbeTObmcLHSIZRPQIch5gyuvdWfM3shUc4uWkVnTLB0_HB1c0jotIscLgIDiK1I5gSc4hvvv9jFarRbYHj9gsLLJKNCtMEY_Gz8aNraxfmwczwq_UO,f2HvwsHUqKQ:APA91bFjlRlKXLGEKm7L0erW4YvwySiBmN43bJQXwhipe8lBVYtEykF8iw88BJk7nmxXUohINmGvuNr0DlgaPAtIvUJCgNURBNKZzZmxl8zJNkO9wEEDEqp-2cKfNf1AWonIw-l5AXv0,dXY-RkA3DlU:APA91bFA2cyoVOZIWsl_JC281g9xAr5I5zMevYlkw8jT5A3SlL-cH6BM202NTwxSye4XXh25L_eNEncb01Vxm1aHv2LRLiw1fl6II-KhbFdXU_Yv25q7pHDRiB8XE3LBuVVDWsBo5VCa'),
(22, 'Sales and Related Occupations\n', 0, '', NULL),
(23, 'Other', 0, '', NULL);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

-- phpMyAdmin SQL Dump
-- version 3.5.8.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 22, 2016 at 09:29 AM
-- Server version: 5.5.31
-- PHP Version: 5.3.29

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `quickjob_v2`
--

-- --------------------------------------------------------

--
-- Table structure for table `jobs_skill`
--

CREATE TABLE IF NOT EXISTS `jobs_skill` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `job_id` varchar(100) NOT NULL,
  `skill_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `job_skill_idx` (`job_id`),
  KEY `job_skill_skill_idx` (`skill_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=144 ;

--
-- Dumping data for table `jobs_skill`
--

INSERT INTO `jobs_skill` (`id`, `job_id`, `skill_id`) VALUES
(1, 'Job23', 1),
(2, 'Job23', 2),
(11, 'Job28', 1),
(12, 'Job28', 2),
(13, 'QJ00010', 1),
(14, 'QJ00010', 2),
(15, 'QJ00011', 1),
(16, 'QJ00012', 1),
(17, 'QJ00013', 3),
(18, 'QJ00014', 1),
(19, 'QJ00014', 2),
(20, 'QJ00019', 1),
(21, 'QJ00020', 1),
(22, 'QJ00021', 2),
(23, 'QJ00022', 1),
(24, 'QJ00022', 2),
(25, 'QJ00040', 1),
(26, 'QJ00040', 2),
(27, 'QJ00041', 1),
(28, 'QJ00041', 2),
(29, 'QJ00048', 1),
(30, 'QJ00048', 2),
(31, 'QJ00049', 1),
(32, 'QJ00049', 2),
(33, 'QJ00050', 1),
(34, 'QJ00050', 2),
(35, 'QJ00051', 1),
(36, 'QJ00051', 2),
(37, 'QJ00052', 1),
(38, 'QJ00052', 2),
(39, 'QJ00053', 1),
(40, 'QJ00054', 1),
(41, 'QJ00055', 2),
(42, 'QJ00056', 1),
(43, 'QJ00057', 1),
(44, 'QJ00057', 2),
(45, 'QJ00059', 1),
(46, 'QJ00060', 1),
(47, 'QJ00061', 1),
(48, 'QJ00062', 1),
(49, 'QJ00063', 1),
(50, 'QJ00064', 1),
(51, 'Job1', 1),
(52, 'QJ00065', 1),
(53, 'QJ00066', 1),
(54, 'QJ00067', 1),
(55, 'QJ00068', 1),
(56, 'QJ00069', 1),
(57, 'QJ00069', 2),
(58, 'QJ00070', 1),
(59, 'QJ00070', 2),
(60, 'QJ00071', 1),
(61, 'QJ00071', 2),
(62, 'QJ00072', 1),
(63, 'QJ00072', 2),
(64, 'QJ00073', 1),
(65, 'QJ00073', 2),
(66, 'QJ00074', 1),
(67, 'QJ00074', 2),
(68, 'QJ00075', 1),
(69, 'QJ00075', 2),
(70, 'QJ00076', 1),
(71, 'QJ00076', 2),
(72, 'QJ00077', 1),
(73, 'QJ00077', 2),
(74, 'QJ00078', 1),
(75, 'QJ00078', 2),
(76, 'QJ00079', 1),
(77, 'QJ00079', 2),
(78, 'QJ00080', 1),
(79, 'QJ00080', 2),
(80, 'QJ00081', 1),
(81, 'QJ00081', 2),
(82, 'QJ00082', 1),
(83, 'QJ00082', 2),
(84, 'QJ00083', 1),
(85, 'QJ00083', 2),
(86, 'QJ00084', 1),
(87, 'QJ00084', 2),
(88, 'QJ00085', 1),
(89, 'QJ00085', 2),
(90, 'QJ00086', 1),
(91, 'QJ00086', 2),
(92, 'QJ00087', 3),
(93, 'QJ00088', 3),
(94, 'QJ00089', 3),
(95, 'QJ00090', 3),
(96, 'QJ00091', 3),
(97, 'QJ00092', 2),
(98, 'QJ00093', 3),
(99, 'QJ00094', 1),
(100, 'QJ00095', 2),
(101, 'QJ00096', 2),
(102, 'QJ00097', 5),
(103, 'QJ00098', 7),
(104, 'QJ00099', 7),
(105, 'QJ000100', 7),
(106, 'QJ000101', 4),
(107, 'QJ000102', 3),
(108, 'QJ000103', 5),
(109, 'QJ000104', 5),
(110, 'QJ000105', 3),
(111, 'QJ000106', 5),
(112, 'QJ000107', 5),
(113, 'QJ000108', 3),
(114, 'QJ000109', 3),
(115, 'QJ000110', 2),
(116, 'QJ000111', 4),
(117, 'QJ000112', 2),
(118, 'QJ000113', 1),
(119, 'QJ000114', 4),
(120, 'QJ000115', 7),
(121, 'QJ000116', 2),
(122, 'QJ000117', 2),
(123, 'QJ000118', 3),
(124, 'QJ000119', 3),
(125, 'QJ000120', 3),
(126, 'QJ000121', 4),
(127, 'QJ000122', 2),
(128, 'QJ000123', 2),
(129, 'QJ000124', 3),
(130, 'QJ000125', 2),
(131, 'QJ000126', 2),
(132, 'QJ000127', 1),
(133, 'QJ000128', 1),
(134, 'QJ000129', 1),
(135, 'QJ000130', 2),
(136, 'QJ000131', 1),
(137, 'QJ000132', 1),
(138, 'QJ000133', 1),
(139, 'QJ000134', 1),
(140, 'QJ000135', 1),
(141, 'QJ000136', 1),
(142, 'QJ000137', 1),
(143, 'QJ000138', 1);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `jobs_skill`
--
ALTER TABLE `jobs_skill`
  ADD CONSTRAINT `job_skill` FOREIGN KEY (`job_id`) REFERENCES `jobs` (`job_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `job_skill_skill` FOREIGN KEY (`skill_id`) REFERENCES `skill_set` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

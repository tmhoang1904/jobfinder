-- phpMyAdmin SQL Dump
-- version 3.5.8.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 22, 2016 at 09:29 AM
-- Server version: 5.5.31
-- PHP Version: 5.3.29

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `quickjob_v2`
--

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

CREATE TABLE IF NOT EXISTS `jobs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `job_name` varchar(200) DEFAULT NULL,
  `job_id` varchar(50) NOT NULL,
  `delete_flag` tinyint(1) DEFAULT '0',
  `start_time` time DEFAULT '00:00:00',
  `end_time` time DEFAULT '23:59:59',
  `employee_type` tinyint(4) DEFAULT '1' COMMENT '1:FullTime,2:Part time,3:QuickJob',
  `salary` int(11) DEFAULT '0',
  `latitude` double DEFAULT '0',
  `longitude` double DEFAULT '0',
  `wd_mon` tinyint(1) DEFAULT '0',
  `wd_tue` tinyint(1) DEFAULT '0',
  `wd_wed` tinyint(1) DEFAULT '0',
  `wd_thi` tinyint(1) DEFAULT '0',
  `wd_fri` tinyint(1) DEFAULT '0',
  `wd_sat` tinyint(1) DEFAULT '0',
  `wd_sun` tinyint(1) DEFAULT '0',
  `job_description` varchar(500) DEFAULT NULL,
  `phone_number` varchar(50) DEFAULT NULL,
  `job_done` tinyint(4) DEFAULT NULL,
  `user_id` varchar(100) NOT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `city` varchar(200) DEFAULT NULL,
  `salary_unit` varchar(50) DEFAULT NULL,
  `mon_start_time` time DEFAULT NULL,
  `mon_end_time` time DEFAULT NULL,
  `tue_start_time` time DEFAULT NULL,
  `tue_end_time` time DEFAULT NULL,
  `wed_start_time` time DEFAULT NULL,
  `wed_end_time` time DEFAULT NULL,
  `thi_start_time` time DEFAULT NULL,
  `thi_end_time` time DEFAULT NULL,
  `fri_start_time` time DEFAULT NULL,
  `fri_end_time` time DEFAULT NULL,
  `sat_start_time` time DEFAULT NULL,
  `sat_end_time` time DEFAULT NULL,
  `sun_start_time` time DEFAULT NULL,
  `sun_end_time` time DEFAULT NULL,
  `available_flag` tinyint(4) NOT NULL DEFAULT '1',
  `requirement` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `job_id` (`job_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=139 ;

--
-- Dumping data for table `jobs`
--

INSERT INTO `jobs` (`id`, `job_name`, `job_id`, `delete_flag`, `start_time`, `end_time`, `employee_type`, `salary`, `latitude`, `longitude`, `wd_mon`, `wd_tue`, `wd_wed`, `wd_thi`, `wd_fri`, `wd_sat`, `wd_sun`, `job_description`, `phone_number`, `job_done`, `user_id`, `start_date`, `end_date`, `city`, `salary_unit`, `mon_start_time`, `mon_end_time`, `tue_start_time`, `tue_end_time`, `wed_start_time`, `wed_end_time`, `thi_start_time`, `thi_end_time`, `fri_start_time`, `fri_end_time`, `sat_start_time`, `sat_end_time`, `sun_start_time`, `sun_end_time`, `available_flag`, `requirement`) VALUES
(1, 'Create soft 1', 'Job1', 1, '00:00:00', '23:59:59', 2, 2000, 20.997175, 105.821823, 1, 1, 1, 1, 1, 1, 0, 'sdasdasd', '2424235', 0, 'admin', '2016-06-01', '2016-06-27', 'Ha Noi', 'hour', '15:07:00', '16:07:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(4, 'Create soft 2', 'Job2', 1, '00:00:00', '23:59:59', 1, 2000, 21.00151, 105.825864, 1, 1, 1, 1, 1, 1, 1, 'sdasdasd', '2424235', 0, 'admin', '2016-06-20', '2016-06-22', 'Ha Noi', 'hour', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(5, 'Job S', 'Job23', 1, '10:00:00', '15:00:00', 1, 25, 20.998087, 105.816494, 1, 1, 1, 1, 1, 1, 0, 'fasfasfasfasf', '2141241', NULL, 'admin', '2016-05-01', '2016-05-16', 'Ha Noi', 'hour', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(9, 'Job Test', 'Job28', 1, '10:00:00', '15:00:00', 1, 25, 21.00281, 105.821077, 1, 1, 1, 1, 1, 1, 0, 'fasfasfasfasf', '2141241', NULL, 'admin', '2016-06-09', '2016-07-07', 'Ha Noi', 'hour', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(10, 'Job 12421 ', 'QJ00010', 1, '10:00:00', '15:00:00', 1, 25, 0, 0, 1, 1, 1, 1, 1, 1, 0, 'fasfasfasfasf', '2141241', NULL, 'admin', '2016-06-08', '2016-07-13', 'Ha Noi', 'hour', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(11, 'Supper job', 'QJ00011', 1, NULL, '18:00:19', 0, 1000, -0.00005770000000000001, 0, 1, 1, 1, 1, 1, 0, 0, 'developer', '99999', NULL, '12', NULL, NULL, 'Ha Noi', 'hour', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(12, 'trung dz', 'QJ00012', 1, '12:14:12', '13:14:15', 2, 1000, 21.0302292, 105.7860526, 1, 0, 0, 0, 0, 0, 0, 'fun', '0934590969', NULL, '12', NULL, NULL, 'Ha Noi', 'hour', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(13, 'xa ', 'QJ00013', 1, '13:30:39', '16:30:41', 2, 500, 21.0259537, 105.7854914, 1, 0, 0, 0, 0, 0, 0, 'pro', '99999', NULL, '12', NULL, NULL, 'Ha Noi', 'hour', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(18, 'tuan vu', 'QJ00014', 1, '10:00:00', '15:00:00', 1, 25, 0, 0, 1, 1, 1, 1, 1, 1, 0, 'fasfasfasfasf', '2141241', NULL, 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(19, 'test', 'QJ00019', 1, NULL, NULL, 0, 20, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 'test thou', NULL, NULL, 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(20, 'hi hi', 'QJ00020', 1, NULL, NULL, 0, 26, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 'test thou', NULL, NULL, 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(21, 'hi hi', 'QJ00021', 1, NULL, NULL, 0, 23, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 'test thou', NULL, NULL, 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(39, 'Job3', 'QJ00022', 1, '10:00:00', '15:00:00', 1, 25, 0, 0, 1, 1, 1, 1, 1, 1, 0, 'fasfasfasfasf', '2141241', NULL, 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(40, 'Job3', 'QJ00040', 1, '10:00:00', '15:00:00', 1, 25, 0, 0, 1, 1, 1, 1, 1, 1, 0, 'fasfasfasfasf', '2141241', NULL, 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(47, 'Job3', 'QJ00041', 1, '10:00:00', '15:00:00', 1, 25, 0, 0, 1, 1, 1, 1, 1, 1, 0, 'fasfasfasfasf', '2141241', NULL, 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(48, 'Job3', 'QJ00048', 1, '10:00:00', '15:00:00', 1, 25, 0, 0, 1, 1, 1, 1, 1, 1, 0, 'fasfasfasfasf', '2141241', NULL, 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(49, 'Job3', 'QJ00049', 1, '10:00:00', '15:00:00', 1, 25, 0, 0, 1, 1, 1, 1, 1, 1, 0, 'fasfasfasfasf', '2141241', NULL, 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(50, 'Job3', 'QJ00050', 1, '10:00:00', '15:00:00', 1, 25, 0, 0, 1, 1, 1, 1, 1, 1, 0, 'fasfasfasfasf', '2141241', NULL, 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(51, 'Job3', 'QJ00051', 1, NULL, NULL, 0, 0, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 'fasfasfasfasf', NULL, NULL, 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(52, 'Job3', 'QJ00052', 1, NULL, NULL, 0, 0, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 'fasfasfasfasf', NULL, NULL, 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(53, 'test Kai', 'QJ00053', 1, NULL, NULL, 0, 20, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 'hi hi', NULL, NULL, 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(54, 'test lan 2', 'QJ00054', 1, NULL, NULL, 0, 20, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 'hi hi', NULL, NULL, 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(55, 'zzzzzzzzzzzz', 'QJ00055', 1, NULL, NULL, 0, 20, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 'hhhhhh', NULL, NULL, 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(56, 'hi', 'QJ00056', 1, NULL, NULL, 0, 20, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 'hhhhhhh', NULL, NULL, 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(58, 'Job3', 'QJ00057', 1, NULL, NULL, 1, 0, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 'fasfasfasfasf', '2141241', NULL, 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(59, 'job', 'QJ00059', 1, NULL, NULL, 0, 0, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, '', NULL, NULL, 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(60, 'test ap by tv', 'QJ00060', 1, NULL, NULL, 0, 20, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, ' Me ', NULL, NULL, 'admin', '2016-07-04', NULL, NULL, 'Year', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(61, 'test ap by tv', 'QJ00061', 1, NULL, NULL, 0, 20, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, ' Me ', NULL, NULL, 'admin', '2016-07-04', NULL, NULL, 'Year', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(62, 'test ap by tv', 'QJ00062', 1, NULL, NULL, 0, 20, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, ' Me ', NULL, NULL, 'admin', '2016-07-04', NULL, NULL, 'Year', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(63, 'test ap by tv', 'QJ00063', 1, NULL, NULL, 0, 20, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, ' Me ', NULL, NULL, 'admin', '2016-07-04', NULL, NULL, 'Year', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(64, 'test ap by tv', 'QJ00064', 1, NULL, NULL, 0, 20, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, ' Me ', NULL, NULL, 'admin', '2016-07-04', NULL, NULL, 'Year', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(65, 'test 13/07', 'QJ00065', 1, NULL, NULL, 3, 50, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, '', NULL, NULL, 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(66, 'try', 'QJ00066', 1, NULL, NULL, 2, 40, 37.78606, -122.4072, 1, 0, 0, 0, 0, 0, 0, 'Des', NULL, NULL, 'admin', '2016-08-19', NULL, NULL, 'Hour', '14:01:00', '14:03:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'Req'),
(67, 'check change weekdays 2', 'QJ00067', 1, NULL, NULL, 2, 50, 37.78606, -122.4072, 1, 1, 1, 0, 0, 0, 0, 'Des', NULL, NULL, 'admin', '2017-07-14', NULL, NULL, 'Hour', '14:01:00', '14:03:00', '12:57:00', '14:03:00', '07:38:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'Req'),
(68, 'check change weekdays 2', 'QJ00068', 1, NULL, NULL, 2, 50, 37.78606, -122.4072, 1, 1, 1, 0, 0, 0, 0, 'Des', NULL, NULL, 'admin', '2017-07-14', NULL, NULL, 'Hour', '14:01:00', '14:03:00', '12:57:00', '14:03:00', '07:38:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'Req'),
(69, 'Job3', 'QJ00069', 1, '10:00:00', '15:00:00', 1, 25, 0, 0, 1, 1, 1, 1, 1, 1, 0, 'fasfasfasfasf', '2141241', NULL, 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(70, 'Job3', 'QJ00070', 1, '10:00:00', '15:00:00', 1, 25, 0, 0, 1, 1, 1, 1, 1, 1, 0, 'fasfasfasfasf', '2141241', NULL, 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(71, 'Job3111', 'QJ00071', 1, '10:00:00', '15:00:00', 1, 25, 0, 0, 1, 1, 1, 1, 1, 1, 0, 'fasfasfasfasf', '2141241', NULL, 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(72, 'Job3111', 'QJ00072', 1, '10:00:00', '15:00:00', 1, 25, 0, 0, 1, 1, 1, 1, 1, 1, 0, 'fasfasfasfasf', '2141241', NULL, 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(73, 'Job3', 'QJ00073', 1, '10:00:00', '15:00:00', 1, 25, 0, 0, 1, 1, 1, 1, 1, 1, 0, 'fasfasfasfasf', '2141241', NULL, 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(74, 'Job3111', 'QJ00074', 1, '10:00:00', '15:00:00', 1, 25, 0, 0, 1, 1, 1, 1, 1, 1, 0, 'fasfasfasfasf', '2141241', NULL, 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(75, 'Job3111', 'QJ00075', 1, '10:00:00', '15:00:00', 1, 25, 0, 0, 1, 1, 1, 1, 1, 1, 0, 'fasfasfasfasf', '2141241', NULL, 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(76, 'Job3111', 'QJ00076', 1, '10:00:00', '15:00:00', 1, 25, 0, 0, 1, 1, 1, 1, 1, 1, 0, 'fasfasfasfasf', '2141241', NULL, 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(77, 'Job3111', 'QJ00077', 1, '10:00:00', '15:00:00', 1, 25, 0, 0, 1, 1, 1, 1, 1, 1, 0, 'fasfasfasfasf', '2141241', NULL, 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(78, 'Job3111', 'QJ00078', 1, '10:00:00', '15:00:00', 1, 25, 0, 0, 1, 1, 1, 1, 1, 1, 0, 'fasfasfasfasf', '2141241', NULL, 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(79, 'Job3111', 'QJ00079', 1, '10:00:00', '15:00:00', 1, 25, 0, 0, 1, 1, 1, 1, 1, 1, 0, 'fasfasfasfasf', '2141241', NULL, 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(80, 'Job3111', 'QJ00080', 1, '10:00:00', '15:00:00', 1, 25, 0, 0, 1, 1, 1, 1, 1, 1, 0, 'fasfasfasfasf', '2141241', NULL, 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(81, 'Job3111', 'QJ00081', 1, '10:00:00', '15:00:00', 1, 25, 0, 0, 1, 1, 1, 1, 1, 1, 0, 'fasfasfasfasf', '2141241', NULL, 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(82, 'Job3111', 'QJ00082', 1, '10:00:00', '15:00:00', 1, 25, 0, 0, 1, 1, 1, 1, 1, 1, 0, 'fasfasfasfasf', '2141241', NULL, 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(83, 'Job3111', 'QJ00083', 1, '10:00:00', '15:00:00', 1, 25, 0, 0, 1, 1, 1, 1, 1, 1, 0, 'fasfasfasfasf', '2141241', NULL, 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(84, 'Job3111', 'QJ00084', 1, '10:00:00', '15:00:00', 1, 25, 0, 0, 1, 1, 1, 1, 1, 1, 0, 'fasfasfasfasf', '2141241', NULL, 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(85, 'Job3111', 'QJ00085', 1, '10:00:00', '15:00:00', 1, 25, 0, 0, 1, 1, 1, 1, 1, 1, 0, 'fasfasfasfasf', '2141241', NULL, 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(86, 'Job3111', 'QJ00086', 1, '10:00:00', '15:00:00', 1, 25, 0, 0, 1, 1, 1, 1, 1, 1, 0, 'fasfasfasfasf', '2141241', NULL, 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(87, 'tiiiiiii', 'QJ00087', 1, '09:04:00', '12:06:00', 3, 20, 21.21372, 105.6803, 0, 0, 0, 0, 0, 0, 0, 'Des', NULL, NULL, 'tuanvu9999', '2016-07-18', '2016-07-29', NULL, 'Hour', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(88, 'tiiiiiii', 'QJ00088', 1, '09:04:00', '12:06:00', 3, 20, 21.21372, 105.6803, 0, 0, 0, 0, 0, 0, 0, 'Des', NULL, NULL, 'tuanvu9999', '2016-07-18', '2016-07-29', NULL, 'Hour', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(89, 'tiiiiiii', 'QJ00089', 1, '09:04:00', '12:06:00', 3, 20, 21.21372, 105.6803, 0, 0, 0, 0, 0, 0, 0, 'Des', NULL, NULL, 'tuanvu9999', '2016-07-18', '2016-07-29', NULL, 'Hour', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(90, 'tiiiiiii', 'QJ00090', 1, '09:04:00', '12:06:00', 3, 20, 21.21372, 105.6803, 0, 0, 0, 0, 0, 0, 0, 'Des', NULL, NULL, 'tuanvu9999', '2016-07-18', '2016-07-29', NULL, 'Hour', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(91, 'hehehe', 'QJ00091', 1, NULL, NULL, 2, 20, 21.21372, 105.6803, 1, 0, 0, 0, 0, 0, 0, 'Kiiiii', NULL, NULL, 'tuanvu9999', '2016-07-19', NULL, NULL, 'Hour', '12:29:00', '12:32:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'Dep trai'),
(92, 'hiho hiho', 'QJ00092', 1, NULL, NULL, 1, 266, 21.21372, 105.6803, 0, 0, 0, 0, 0, 0, 0, 'Heeee', NULL, NULL, 'tuanvu9999', '2016-01-20', NULL, NULL, 'Year', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'Jurors'),
(93, 'keeeeeeee', 'QJ00093', 1, NULL, NULL, 2, 100, 21.21372, 105.6803, 1, 0, 0, 1, 0, 0, 0, 'Kiki', NULL, NULL, 'tuanvu9999', '2015-07-04', NULL, NULL, 'Hour', '12:38:00', '13:38:00', NULL, NULL, NULL, NULL, '12:27:00', '13:37:00', NULL, NULL, NULL, NULL, NULL, NULL, 1, 'Kiki'),
(94, 'd?ch khan', 'QJ00094', 0, NULL, NULL, 3, 2000, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 'Jjjjjhjgh', NULL, NULL, 'admin', NULL, NULL, NULL, 'Hour', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, NULL),
(95, 'gh?', 'QJ00095', 0, NULL, NULL, 3, 20, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, '50', NULL, NULL, 'admin', NULL, NULL, NULL, 'Hour', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL),
(96, 'gghh', 'QJ00096', 0, NULL, NULL, 3, 20, 21.00601, 105.8012, 0, 0, 0, 0, 0, 0, 0, 'He', NULL, NULL, 'admin', NULL, NULL, NULL, 'Hour', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(97, 'Tu?n ', 'QJ00097', 0, NULL, NULL, 3, 200, 21.0084, 105.8019, 0, 0, 0, 0, 0, 0, 0, 'Ke?t', NULL, NULL, 'vutuan ', NULL, NULL, NULL, 'Hour', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(98, 'kkiki', 'QJ00098', 0, NULL, NULL, 3, 2000, 21.00734, 105.803, 0, 0, 0, 0, 0, 0, 0, '1000\n', NULL, NULL, 'vutuan ', NULL, NULL, NULL, 'Hour', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL),
(99, 'kkiki', 'QJ00099', 0, NULL, NULL, 3, 2000, 21.00734, 105.803, 0, 0, 0, 0, 0, 0, 0, '1000\n', NULL, NULL, 'vutuan ', NULL, NULL, NULL, 'Hour', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(100, 'kkikikiii', 'QJ000100', 0, NULL, NULL, 3, 2000, 21.00734, 105.803, 0, 0, 0, 0, 0, 0, 0, '1000\n', NULL, NULL, 'vutuan ', NULL, NULL, NULL, 'Hour', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(101, 'make', 'QJ000101', 0, NULL, NULL, 3, 20, 21.0084, 105.8019, 0, 0, 0, 0, 0, 0, 0, 'Hj', NULL, NULL, 'vutuan ', NULL, NULL, NULL, 'Hour', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(102, 'ghi bk', 'QJ000102', 0, NULL, NULL, 3, 20, 21.0084, 105.8019, 0, 0, 0, 0, 0, 0, 0, 'Kho', NULL, NULL, 'vutuan ', NULL, NULL, NULL, 'Hour', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(103, 'ghen k', 'QJ000103', 0, NULL, NULL, 3, 20, 21.00779, 105.8011, 0, 0, 0, 0, 0, 0, 0, 'Hj', NULL, NULL, 'vutuan ', NULL, NULL, NULL, 'Hour', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(104, 'ghen k', 'QJ000104', 0, NULL, NULL, 3, 20, 21.00779, 105.8011, 0, 0, 0, 0, 0, 0, 0, 'Hj', NULL, NULL, 'vutuan ', NULL, NULL, NULL, 'Hour', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(105, 'Ok', 'QJ000105', 0, '13:18:00', '14:22:00', 3, 20, 21.0089, 105.8013, 0, 0, 0, 0, 0, 0, 0, 'Khan', NULL, NULL, 'vutuan ', '2016-07-22', '2016-07-22', NULL, 'Hour', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(106, 'cai gì v', 'QJ000106', 0, NULL, NULL, 2, 200, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 'H?n', NULL, NULL, 'vutuan ', '2016-06-22', NULL, NULL, 'Hour', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(107, 'kohoeu', 'QJ000107', 0, NULL, NULL, 2, 300, 21.00695, 105.7986, 1, 0, 0, 0, 0, 0, 0, 'Hehe', NULL, NULL, 'vutuan ', '2018-10-22', NULL, NULL, 'Hour', '12:38:00', '14:40:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'Keke'),
(108, 'Hihi', 'QJ000108', 0, '18:17:00', '18:18:00', 3, 26, 21.00769, 105.8014, 0, 0, 0, 0, 0, 0, 0, 'To', NULL, NULL, 'vutuan ', '2016-07-20', '2016-07-23', NULL, 'Hour', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(109, 'hello', 'QJ000109', 0, '00:40:00', '23:41:00', 3, 200, 51.50972, -0.1342374, 0, 0, 0, 0, 0, 0, 0, 'Ok ', NULL, NULL, 'vutuan ', '2016-07-24', '2016-07-26', NULL, 'Hour', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(110, 'hello', 'QJ000110', 0, NULL, NULL, 2, 40, -26.20568, 28.04682, 1, 0, 0, 0, 0, 0, 0, 'Des', NULL, NULL, 'vutuan ', '2016-06-24', NULL, NULL, 'Hour', '01:48:00', '01:49:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'Req'),
(111, 'hello', 'QJ000111', 0, NULL, NULL, 1, 20, -26.20415, 28.0473, 0, 0, 0, 0, 0, 0, 0, 'Des', NULL, NULL, 'vutuan ', '2015-06-24', NULL, NULL, 'Year', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'Ques'),
(112, 'Avatar', 'QJ000112', 0, '17:50:00', '17:52:00', 3, 20000, 21.22424, 105.6735, 0, 0, 0, 0, 0, 0, 0, 'Dep trai', NULL, NULL, 'admin', '2016-08-07', '2016-08-08', NULL, 'Hour', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(113, 'Titanic â', 'QJ000113', 0, '02:14:00', '02:14:00', 3, 20, 21.00662, 105.8657, 0, 0, 0, 0, 0, 0, 0, 'We don''t talk anymore ', NULL, NULL, 'admin', '2016-08-06', '2016-08-09', NULL, 'Hour', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(114, 'Parting', 'QJ000114', 0, NULL, NULL, 2, 100, 21.00702, 105.8652, 1, 0, 0, 0, 0, 0, 0, 'Description', NULL, NULL, 'admin', '2015-02-08', NULL, NULL, 'Hour', '01:15:00', '02:24:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'Really '),
(115, 'Filling', 'QJ000115', 0, NULL, NULL, 1, 20, 21.00646, 105.862, 0, 0, 0, 0, 0, 0, 0, 'Hodnkddb', NULL, NULL, 'admin', '2006-12-08', NULL, NULL, 'Year', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'Hood bonds'),
(116, 'Death ', 'QJ000116', 0, '11:35:00', '10:36:00', 3, 28, 14.71452, 102.0718, 0, 0, 0, 0, 0, 0, 0, 'Tttttttt', NULL, NULL, 'admin', '2016-08-06', '2016-08-08', NULL, 'Hour', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(117, 'Death ', 'QJ000117', 0, '11:35:00', '10:36:00', 3, 28, 14.71452, 102.0718, 0, 0, 0, 0, 0, 0, 0, 'Tttttttt', NULL, NULL, 'admin', '2016-08-06', '2016-08-08', NULL, 'Hour', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(118, 'delegations ', 'QJ000118', 0, NULL, NULL, 1, 20, 21.22424, 105.6735, 0, 0, 0, 0, 0, 0, 0, 'Shiv. G to kjc ', NULL, NULL, 'admin', '2016-08-07', NULL, NULL, 'Year', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'Gil he knows what to say it was not immediately '),
(119, 'The best ', 'QJ000119', 0, '12:41:00', '11:32:00', 3, 20, 21.17534, 105.7308, 0, 0, 0, 0, 0, 0, 0, 'The best ', NULL, NULL, 'admin', '2016-08-08', '2016-08-08', NULL, 'Hour', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(120, 'The only one ', 'QJ000120', 0, '01:41:00', '17:01:00', 3, 256, 38.79459, 106.5348, 0, 0, 0, 0, 0, 0, 0, 'The fact I have a nice person and a few ', NULL, NULL, 'admin', '2016-08-08', '2016-10-18', NULL, 'Hour', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(121, 'The fact is a very happy ', 'QJ000121', 0, '16:47:00', '12:47:00', 3, 208, 21.21222, 105.684, 0, 0, 0, 0, 0, 0, 0, 'The only thing is I don''t think it''s time I try not too late for class ', NULL, NULL, 'admin', '2016-06-14', '2016-08-08', NULL, 'Hour', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(122, 'The best of me ', 'QJ000122', 0, '18:23:00', '15:44:00', 3, 1000, 21.22424, 105.6735, 0, 0, 0, 0, 0, 0, 0, 'The best of me to do it all in my head hurts and my dad and my sister and my dad ', NULL, NULL, '3032059036', '2016-07-12', '2016-08-31', NULL, 'Hour', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(123, 'The fact best of me to is so ', 'QJ000123', 0, '13:10:00', '13:10:00', 3, 2605, 21.22424, 105.6735, 0, 0, 0, 0, 0, 0, 0, 'The best of me to is so cute when you he is ', NULL, NULL, '1657605701228705', '2016-08-09', '2016-08-10', NULL, 'Hour', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(124, 'Aerospace Engineering and Operations Technici', 'QJ000124', 0, '21:43:00', '21:43:00', 3, 50, 21.21222, 105.684, 0, 0, 0, 0, 0, 0, 0, 'The I''m so going out with me my ', NULL, NULL, 'admin', '2016-08-25', '2016-08-27', NULL, 'Hour', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(125, 'Aerospace Engineering and Operations Technici', 'QJ000125', 0, '01:19:00', '01:19:00', 3, 26, 21.22424, 105.6735, 0, 0, 0, 0, 0, 0, 0, 'The best ', NULL, NULL, 'admin', '2016-08-24', '2016-08-25', NULL, 'Hour', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(126, 'Photographers', 'QJ000126', 0, '19:04:00', '20:05:00', 3, 25, 43.65638, -79.43554, 0, 0, 0, 0, 0, 0, 0, 'Need a photographer', NULL, NULL, 'quickjob001', '2016-08-27', '2016-08-27', NULL, 'Hour', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, NULL),
(127, 'Aerospace Engineers ', 'QJ000127', 0, '15:48:00', '22:48:00', 3, 50, 43.65092, -79.40145, 0, 0, 0, 0, 0, 0, 0, 'anc', NULL, NULL, 'quickjob001', '2016-09-02', '2016-09-02', NULL, 'Hour', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(128, 'Photographers', 'QJ000128', 0, '15:48:00', '22:48:00', 3, 50, 43.65092, -79.40145, 0, 0, 0, 0, 0, 0, 0, 'anc', NULL, NULL, 'quickjob001', '2016-09-02', '2016-09-02', NULL, 'Hour', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(129, 'Automotive Engineers ', 'QJ000129', 0, '18:40:00', '17:43:00', 3, 50, 43.6514, -79.47599, 0, 0, 0, 0, 0, 0, 0, 'changing tire', NULL, NULL, 'quickjob001', '2016-09-02', '2016-09-02', NULL, 'Hour', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(130, 'Photographers', 'QJ000130', 0, '04:51:00', '04:52:00', 3, 50, 43.65096, -79.40123, 0, 0, 0, 0, 0, 0, 0, 'need', NULL, NULL, 'quickjob001', '2016-10-18', '2016-10-18', NULL, 'Hour', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(131, 'Aerospace Engineering and Operations Technici', 'QJ000131', 0, '16:31:00', '16:31:00', 3, 2000, 21.22424, 105.6735, 0, 0, 0, 0, 0, 0, 0, 'Tuan ', NULL, NULL, 'admin', '2016-11-01', '2016-11-02', NULL, 'Hour', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(132, 'Aerospace Engineering and Operations Technici', 'QJ000132', 0, '17:44:00', '17:44:00', 3, 123456789, 21.22424, 105.6735, 0, 0, 0, 0, 0, 0, 0, 'Tuan ', NULL, NULL, 'admin', '2016-10-31', '2016-11-01', NULL, 'Hour', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(133, 'Aerospace Engineers ', 'QJ000133', 0, '17:46:00', '17:46:00', 3, 888, 21.22424, 105.6735, 0, 0, 0, 0, 0, 0, 0, 'Tuan ', NULL, NULL, 'admin', '2016-10-31', '2016-11-03', NULL, 'Hour', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(134, 'Aerospace Engineers ', 'QJ000134', 0, '17:46:00', '17:46:00', 3, 888, 21.22424, 105.6735, 0, 0, 0, 0, 0, 0, 0, 'Tuan ', NULL, NULL, 'admin', '2016-10-31', '2016-11-03', NULL, 'Hour', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(135, 'Aerospace Engineering and Operations Technici', 'QJ000135', 0, NULL, NULL, 3, 100, 21.22424, 105.6735, 0, 0, 0, 0, 0, 0, 0, 'Tuan ', NULL, NULL, 'admin', NULL, NULL, NULL, 'Hour', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(136, 'Landscape Architects ', 'QJ000136', 0, '07:38:00', '08:38:00', 3, 50, 43.65444, -79.3807, 0, 0, 0, 0, 0, 0, 0, '', NULL, NULL, 'quickjob001', '2016-11-17', '2016-11-17', NULL, 'Hour', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(137, 'Aerospace Engineering and Operations Technici', 'QJ000137', 0, NULL, NULL, 3, 100, 21.22424, 105.6735, 0, 0, 0, 0, 0, 0, 0, 'Tuan the fact he is a not is ', NULL, NULL, 'admin', NULL, NULL, NULL, 'Hour', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(138, 'Aerospace Engineers ', 'QJ000138', 0, NULL, NULL, 3, 69, 21.22424, 105.6735, 0, 0, 0, 0, 0, 0, 0, 'The ', NULL, NULL, 'admin', NULL, NULL, NULL, 'Hour', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

-- phpMyAdmin SQL Dump
-- version 3.5.8.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 22, 2016 at 09:27 AM
-- Server version: 5.5.31
-- PHP Version: 5.3.29

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `quickjob_v2`
--

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

CREATE TABLE IF NOT EXISTS `employees` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `profile_id` varchar(100) NOT NULL,
  `user_name` varchar(100) NOT NULL,
  `available_flag` tinyint(1) DEFAULT '0',
  `img_url` varchar(100) DEFAULT NULL,
  `start_time` time DEFAULT '00:00:00',
  `phone_number` varchar(50) NOT NULL DEFAULT '',
  `end_time` time DEFAULT '23:59:59',
  `experience` text,
  `delete_flag` tinyint(1) DEFAULT '0',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `latitude` double DEFAULT '0',
  `longitude` double DEFAULT '0',
  `employee_type` tinyint(4) DEFAULT '1' COMMENT '1:Full Time,2:Part Time,3: Temporary',
  `email` varchar(45) DEFAULT NULL,
  `salary` int(11) DEFAULT NULL,
  `latitude_address` double DEFAULT NULL,
  `longitude_addres` double DEFAULT NULL,
  `address` varchar(300) DEFAULT NULL,
  `wd_mon` tinyint(1) DEFAULT '0',
  `wd_tue` tinyint(1) DEFAULT '0',
  `wd_wed` tinyint(1) DEFAULT '0',
  `wd_thi` tinyint(1) DEFAULT '0',
  `wd_fri` tinyint(1) DEFAULT '0',
  `wd_sat` tinyint(1) DEFAULT '0',
  `wd_sun` tinyint(1) DEFAULT '0',
  `password` varchar(45) DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `salary_unit` varchar(50) DEFAULT NULL,
  `description` varchar(300) DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `mon_start_time` time DEFAULT NULL,
  `mon_end_time` time DEFAULT NULL,
  `tue_start_time` time DEFAULT NULL,
  `tue_end_time` time DEFAULT NULL,
  `wed_start_time` time DEFAULT NULL,
  `wed_end_time` time DEFAULT NULL,
  `thi_start_time` time DEFAULT NULL,
  `thi_end_time` time DEFAULT NULL,
  `fri_start_time` time DEFAULT NULL,
  `fri_end_time` time DEFAULT NULL,
  `sat_start_time` time DEFAULT NULL,
  `sat_end_time` time DEFAULT NULL,
  `sun_start_time` time DEFAULT NULL,
  `sun_end_time` time DEFAULT NULL,
  `skill_desc` varchar(500) DEFAULT NULL,
  `profile_name` varchar(60) DEFAULT NULL,
  `user_id` varchar(100) DEFAULT NULL,
  `noti_token` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id_UNIQUE` (`profile_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=108 ;

--
-- Dumping data for table `employees`
--

INSERT INTO `employees` (`id`, `profile_id`, `user_name`, `available_flag`, `img_url`, `start_time`, `phone_number`, `end_time`, `experience`, `delete_flag`, `update_time`, `latitude`, `longitude`, `employee_type`, `email`, `salary`, `latitude_address`, `longitude_addres`, `address`, `wd_mon`, `wd_tue`, `wd_wed`, `wd_thi`, `wd_fri`, `wd_sat`, `wd_sun`, `password`, `start_date`, `salary_unit`, `description`, `end_date`, `mon_start_time`, `mon_end_time`, `tue_start_time`, `tue_end_time`, `wed_start_time`, `wed_end_time`, `thi_start_time`, `thi_end_time`, `fri_start_time`, `fri_end_time`, `sat_start_time`, `sat_end_time`, `sun_start_time`, `sun_end_time`, `skill_desc`, `profile_name`, `user_id`, `noti_token`) VALUES
(1, 'admin', 'admin', 1, NULL, '07:00:00', '', '12:00:00', NULL, 0, '2016-07-07 15:37:18', 21.00860670267635, 105.8020272558682, 2, 'admin@localhost.com', NULL, NULL, NULL, NULL, 1, 1, 1, 1, 1, 1, 1, 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(7, 'admin2', 'admin2 43 34', 1, NULL, '07:00:00', '1223123123123', '18:00:00', NULL, 0, '2016-07-13 03:27:49', 0, 0, 1, 'admin2@localhost.com', NULL, NULL, NULL, NULL, 1, 1, 1, 1, 0, 0, 0, 'admin2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(10, 'test1241', 'test24', 1, NULL, '08:00:00', '54546456', '10:00:00', '412412', 0, '2016-07-13 03:27:49', 0, 0, 1, 'a@faf.com', 12412, NULL, NULL, '214124', 1, 1, 1, 0, 0, 0, 1, '123456', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(15, 'tester', 'test24123', 1, NULL, '08:00:00', '54546456', '10:00:00', '412412', 0, '2016-07-13 03:27:49', 0, 0, 1, 'a@faf.com', 12412, NULL, NULL, '214124', 1, 1, 1, 0, 0, 0, 1, '123456', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(25, 'test124142', 'testnew', 1, NULL, '08:00:00', '54546456', '10:00:00', '412412', 0, '2016-07-13 03:27:49', 0, 0, 1, 'a@faf.com', 12412, NULL, NULL, '214124', 1, 1, 1, 0, 0, 0, 1, '1234564', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(28, 'test12345', 'test123456', 1, NULL, '08:00:00', '54546456', '10:00:00', '412412', 0, '2016-07-13 03:27:49', 0, 0, 1, 'a@faf.com', 12412, NULL, NULL, '214124', 1, 1, 1, 0, 0, 0, 1, '123456', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(32, '12', '12', 1, NULL, '02:41:08', '12', '14:41:11', '12', 0, '2016-07-13 03:27:49', 0, 0, 1, '12', 12, NULL, NULL, 'Cool Britannia - 225-229 Piccadilly, London W1J 9HR, United Kingdom', 1, 1, 0, 0, 0, 0, 0, '12', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(35, '123', '12', 1, NULL, '02:41:08', '12', '14:41:11', '12', 0, '2016-07-13 03:27:49', 51.5099027, -0.1336343, 1, '12', 12, NULL, NULL, 'Cool Britannia - 225-229 Piccadilly, London W1J 9HR, United Kingdom', 1, 1, 0, 0, 0, 0, 0, '12', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(36, 'Trung DZ', 'meobeoxu', 1, NULL, '23:51:06', '0934590969', '03:51:09', 'vl', 0, '2016-07-13 03:27:49', NULL, NULL, 1, 'trungbt.developer@gmail.com', 1000000, NULL, NULL, '48 V? Tr?ng Ph?ng - 48 V? Tr?ng Ph?ng, Thanh Xuân Trung, Thanh Xuân, Hà N?i, Vietnam', 1, 0, 0, 0, 0, 0, 0, 'trung123', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(38, 'anhdt12', 'anh doan', 1, NULL, '08:00:00', '54546456', '10:00:00', '412412', 0, '2016-07-13 03:27:49', 0, 0, 1, 'a@faf.com', 12412, NULL, NULL, '214124', 1, 1, 1, 0, 0, 0, 1, '123456', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(39, 'TrungVKL', 'Trung B? ??if', 1, NULL, '01:21:49', '0934590969', '02:21:52', 'pro', 0, '2016-07-13 03:27:49', 12.23783066802412, 109.1961304147431, 1, 'trungbt.lmh@gmail.com', 10000, NULL, NULL, '46 V? Tr?ng Ph?ng - 46 V? Tr?ng Ph?ng, Thanh Xuân Trung, Thanh Xuân, Hà N?i, Vietnam', 1, 0, 0, 0, 0, 0, 0, '123456', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(46, 'long1234', 'Long Long', 1, NULL, '12:55:02', '6478885555', '00:00:05', NULL, 0, '2016-07-13 03:27:49', 43.65280981179666, -79.43828006282396, 1, 'longlong@fonhills.com', 25, NULL, NULL, 'Dufferin Mall - 900 Dufferin St, Toronto, ON M6H 4A9, Canada', 1, 0, 0, 0, 0, 0, 0, 'Long1234', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(57, 'nam1234', 'nam nam long', 1, NULL, '03:18:53', '6475552222', '22:20:07', NULL, 0, '2016-07-13 03:27:49', 43.65289933052254, -79.4383369759465, 1, 'namlong123@gmail.com', 25, NULL, NULL, 'Dufferin Mall - 900 Dufferin St, Toronto, ON M6H 4A9, Canada', 1, 1, 1, 1, 1, 1, 0, 'Long123456', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(60, 'long12', 'nam long nam', 1, NULL, '02:12:49', '2125556666', '22:30:53', NULL, 0, '2016-07-13 03:27:49', 43.65301759917629, -79.43803950220294, 1, 'nam@looggg.com', 20, NULL, NULL, 'Dufferin Mall - 900 Dufferin St, Toronto, ON M6H 4A9, Canada', 1, 0, 0, 0, 0, 0, 0, 'Long1234@', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(61, 'tuanvu2605', 'tuan vu', 1, NULL, '10:29:42', '0989848356', '11:39:49', 'jjk', 0, '2016-07-13 03:27:49', 21.0277644, 105.8341598, 1, 's2.4home@gmail.com', 12222, NULL, NULL, 'Hanoi - Hanoi, Hoàn Ki?m, Hanoi, Vietnam', 0, 1, 1, 1, 1, 0, 0, '12345678', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(62, 'long999', 'Long', 1, NULL, '03:26:53', '6475558855', '22:30:56', NULL, 0, '2016-07-13 03:27:49', 43.65281232636762, -79.43838148385234, 1, 'long999@longeee.com', 20, NULL, NULL, 'Dufferin Mall - 900 Dufferin St, Toronto, ON M6H 4A9, Canada', 1, 0, 0, 0, 0, 0, 0, 'Long1234', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(63, '8fa3a85e-0e12-40d8-9846-a2f7cb7f8a31', 'admin', 1, NULL, NULL, '54546456', NULL, NULL, 0, '2016-11-22 08:14:25', NULL, NULL, 1, 'a@faf.com', 124, NULL, NULL, '214124', 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'admin', 'admin', NULL),
(64, 'cbf05259-1163-44a3-9fb5-adc0a5d7a8e0', 'tuan vu ', 0, NULL, NULL, '0999999999', NULL, NULL, 0, '2016-07-19 10:57:55', NULL, NULL, 1, 'tv@gmail.com', 20, NULL, NULL, 'Hun', 1, 0, 0, 0, 0, 0, 0, NULL, '2016-07-14', 'Hour', 'Haaaaaaaaaaaaaaa\n', NULL, '17:56:00', '17:58:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Heeeeeeeee', 'tuanhiiii', 'tuanvu9999', NULL),
(65, 'afd53882-502b-48f7-a430-0dae1a55e820', 'tuanvu', 1, NULL, NULL, '123456789', NULL, NULL, 0, '2016-08-05 06:40:24', NULL, NULL, 1, 'Gmail@yahoo.com', 200, NULL, NULL, 'hn ', 1, 0, 0, 0, 0, 0, 0, NULL, '2015-07-21', 'Hour', 'Hi', NULL, '01:21:00', '01:46:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Hi', 'profile tuanvu', 'vutuan ', NULL),
(66, '3de4dabc-cbd5-4315-a3cb-ef86188043bc', 'tuanvu', 0, NULL, NULL, '123456789', NULL, NULL, 0, '2016-07-22 08:17:44', NULL, NULL, 1, 'Gmail@yahoo.com', 20, NULL, NULL, 'hn ', 1, 0, 0, 0, 0, 0, 0, NULL, '2016-07-22', 'Hour', 'Hi', NULL, '14:16:00', '16:18:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Hi', 'hihihi', 'vutuan ', NULL),
(67, '80c8bbad-91a1-4e4c-8f40-b2ff14a5f307', 'tuanvu', 1, NULL, NULL, '123456789', NULL, NULL, 0, '2016-08-05 06:47:37', NULL, NULL, 1, 'Gmail@yahoo.com', NULL, NULL, NULL, 'hn ', 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, 'Hi', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Hi', NULL, 'vutuan ', NULL),
(68, '0088a760-d31f-4039-8ec5-e7db3eb6544a', 'tuanvu', 0, NULL, NULL, '0989888888', NULL, NULL, 0, '2016-07-26 07:33:41', NULL, NULL, 1, 'zxc@vb.com', 20, NULL, NULL, 'ha noi', 1, 0, 0, 0, 0, 0, 0, NULL, '2014-10-26', 'Hour', 'K', NULL, '14:29:00', '14:33:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'K', 'kcj', 'newacc', NULL),
(69, '81032f63-2db0-4943-a0f4-ae125f02504d', 'admin', 1, NULL, NULL, '01929300', NULL, NULL, 0, '2016-08-25 08:22:22', NULL, NULL, 1, 'admin@gmail.com', 2000, NULL, NULL, 'admin Home', 1, 0, 0, 0, 0, 0, 0, NULL, '2016-07-08', 'Hour', 'djfafkjkafj', NULL, '01:15:00', '02:24:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'She gone', 'Prototypes ', 'admin', NULL),
(70, '1c83781e-9810-475d-9ee4-53200197d666', 'admin', 1, NULL, NULL, '01929300', NULL, NULL, 0, '2016-08-25 07:43:13', NULL, NULL, 1, 'admin@gmail.com', 169, NULL, NULL, 'admin Home', 1, 0, 0, 0, 0, 0, 0, NULL, '2016-08-07', 'Hour', NULL, NULL, '11:34:00', '11:35:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'She gone', 'delegates ', 'admin', NULL),
(71, 'a8027e0b-37c6-4dfd-8f91-b1a194d99c04', 'admin', 1, NULL, NULL, '01929300', NULL, NULL, 0, '2016-11-22 07:31:47', NULL, NULL, 1, 'admin@gmail.com', 20, NULL, NULL, 'admin Home', 1, 0, 0, 0, 0, 0, 0, NULL, '2015-08-08', 'Hour', 'djfafkjkafj', NULL, '09:03:00', '10:05:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Theeee', 'admin', NULL),
(72, 'a3d15434-9f25-414b-9de0-c39ecc1d0d0f', 'admin', 1, NULL, NULL, '01929300', NULL, NULL, 0, '2016-11-22 07:31:54', NULL, NULL, 1, 'admin@gmail.com', 20, NULL, NULL, 'admin Home', 1, 0, 0, 0, 0, 0, 0, NULL, '2015-08-08', 'Hour', NULL, NULL, '08:06:00', '10:08:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'admin', NULL),
(73, '922e4479-e507-433d-b6ca-106c0d25e47a', 'admin', 0, NULL, NULL, '01929300', NULL, NULL, 0, '2016-08-09 11:02:02', -33.72434, -15.99609, 1, 'admin@gmail.com', 1000, NULL, NULL, '', 0, 0, 0, 0, 1, 0, 0, NULL, '2016-08-09', 'Hour', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '17:59:00', '19:04:00', NULL, NULL, NULL, NULL, NULL, 'The fact is a very ', 'admin', NULL),
(74, 'e177a2d1-1df2-4ba5-a959-634eef527591', 'admin', 0, NULL, NULL, '01929300', NULL, NULL, 0, '2016-08-09 11:05:27', -14.59941, -28.67315, 1, 'admin@gmail.com', 20, NULL, NULL, 'Atlantic Ocean', 1, 0, 0, 0, 0, 0, 0, NULL, '2016-08-09', 'Hour', NULL, NULL, '18:58:00', '18:05:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'The best thing ', 'admin', NULL),
(75, '95b26d8e-f4a8-4c02-ac9a-9e425a05872e', 'admin', 1, NULL, NULL, '01929300', NULL, NULL, 0, '2016-10-13 06:44:54', -33.72434, -15.99609, 1, 'admin@gmail.com', 20, NULL, NULL, 'South Atlantic Ocean', 1, 0, 0, 0, 0, 0, 0, NULL, '2016-08-08', 'Hour', NULL, NULL, '18:58:00', '18:05:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'The only ', 'admin', NULL),
(76, '9b274c8e-c46b-41bc-840f-8b58a3e378fd', 'admin', 0, NULL, NULL, '01929300', NULL, NULL, 0, '2016-08-09 11:08:53', -14.59941, -28.67315, 1, 'admin@gmail.com', 23, NULL, NULL, 'Atlantic Ocean', 1, 0, 0, 0, 0, 0, 0, NULL, '2016-08-08', 'Hour', NULL, NULL, '18:07:00', '18:08:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'The ', 'admin', NULL),
(77, '8f6ef276-02de-4d30-a9cd-a634824be018', 'admin', 0, NULL, NULL, '01929300', NULL, NULL, 0, '2016-08-09 11:09:44', -14.59941, -28.67315, 1, 'admin@gmail.com', 23, NULL, NULL, 'Atlantic Ocean', 1, 0, 0, 0, 0, 0, 0, NULL, '2016-08-08', 'Hour', NULL, NULL, '18:07:00', '18:08:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'The ', 'admin', NULL),
(78, '4c341d0e-2c26-4b46-bb24-767fb4de8897', 'admin', 0, NULL, NULL, '01929300', NULL, NULL, 0, '2016-08-09 11:10:16', -14.59941, -28.67315, 1, 'admin@gmail.com', 23, NULL, NULL, 'Atlantic Ocean', 1, 0, 0, 0, 0, 0, 0, NULL, '2016-08-08', 'Hour', NULL, NULL, '18:07:00', '18:08:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'The ', 'admin', NULL),
(79, '151bee69-ed06-47cc-a6af-51152348a5b9', 'admin', 0, NULL, NULL, '01929300', NULL, NULL, 0, '2016-08-09 11:11:12', NULL, NULL, 1, 'admin@gmail.com', 1, NULL, NULL, '', 1, 0, 0, 0, 0, 0, 0, NULL, '2016-08-08', 'Hour', NULL, NULL, '18:10:00', '18:11:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'I ', 'admin', NULL),
(80, 'd7c65da7-135f-4f65-8101-8612690f4f63', 'Tuấn Vũ', 0, NULL, NULL, '2088855888', NULL, NULL, 0, '2016-08-10 17:13:54', -33.72434, -15.99609, 1, 'Gjbb@hhhhhh.com', 2085, NULL, NULL, 'South Atlantic Ocean', 1, 0, 0, 0, 0, 0, 0, NULL, '2016-07-11', 'Hour', NULL, NULL, '00:12:00', '00:13:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'The only thing ', '1657605701228705', NULL),
(81, 'e6f041f9-abfe-40f0-be61-a7c6eebbbeac', 'admin', 0, NULL, NULL, '01929300', NULL, NULL, 0, '2016-08-25 05:49:09', NULL, NULL, 1, 'admin@gmail.com', NULL, NULL, NULL, '', 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'admin', NULL),
(82, 'b1a9ea50-cc80-4851-ac55-21bc235ea5e5', 'admin', 0, NULL, NULL, '01929300', NULL, NULL, 0, '2016-08-25 05:50:39', NULL, NULL, 1, 'admin@gmail.com', NULL, NULL, NULL, '', 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'admin', NULL),
(83, 'e30d7dc0-056c-4d3a-bcaa-482a370fbf00', 'quickjob001', 0, NULL, NULL, '0208080008', NULL, NULL, 0, '2016-11-17 13:09:32', 43.65092, -79.40145, 1, 'g@mail.com', 50, NULL, NULL, '165 Grange Ave, Toronto, ON M5T 2V5, Canada', 1, 0, 0, 0, 0, 0, 0, NULL, '2016-09-08', 'Hour', NULL, NULL, '07:51:00', '10:51:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'good', 'Photographer', 'quickjob001', NULL),
(84, '8d6fea7c-21e2-48f6-a169-eb6bcdeacef3', 'quickjob001', 1, NULL, NULL, '0208080008', NULL, NULL, 0, '2016-11-16 08:15:57', 43.65033, -79.39986, 1, 'g@mail.com', 50, NULL, NULL, '75 Augusta Square, Toronto, ON M5T 2K5, Canada', 0, 0, 0, 0, 0, 0, 0, NULL, NULL, 'Hour', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'driver', 'quickjob001', NULL),
(85, '1c6bec10-79e5-49a0-bbed-55177e347e21', 'quickjob001', 1, NULL, NULL, '0208080008', NULL, NULL, 0, '2016-11-16 08:15:57', 43.65033, -79.39986, 1, 'g@mail.com', 50, NULL, NULL, '75 Augusta Square, Toronto, ON M5T 2K5, Canada', 0, 0, 0, 0, 0, 0, 0, NULL, NULL, 'Hour', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'writer', 'quickjob001', NULL),
(86, '1c4c3516-39d4-4c05-b2e3-1e22e15557fb', 'admin', 0, NULL, NULL, '01929300', NULL, NULL, 0, '2016-09-23 08:20:22', NULL, NULL, 1, 'admin@gmail.com', NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'admin', NULL),
(87, 'ae11d8fa-985c-4b97-8408-cb6466e99512', 'admin', 0, NULL, NULL, '01929300', NULL, NULL, 0, '2016-09-23 08:23:51', NULL, NULL, 1, 'admin@gmail.com', NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'admin', NULL),
(88, 'ec5d06c6-40f2-490b-9647-c072723d7d70', 'admin', 0, NULL, NULL, '01929300', NULL, NULL, 0, '2016-09-23 08:24:13', NULL, NULL, 1, 'admin@gmail.com', NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', 'admin', NULL),
(89, 'ede86a1a-b6cc-448a-b2fd-ee8002df4f56', 'admin', 0, NULL, NULL, '01929300', NULL, NULL, 0, '2016-09-23 08:24:18', NULL, NULL, 1, 'admin@gmail.com', NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'admin', NULL),
(90, '60167236-62b0-4882-80b7-4befa2bb0057', 'admin', 0, NULL, NULL, '01929300', NULL, NULL, 0, '2016-11-01 06:47:44', -33.72434, -15.99609, 1, 'admin@gmail.com', 123456789, NULL, NULL, 'South Atlantic Ocean', 1, 0, 0, 0, 0, 0, 0, NULL, '2016-11-01', 'Hour', NULL, NULL, '13:46:00', '13:47:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'She gone', 'test', 'admin', NULL),
(91, '770e24eb-bf8c-42e4-973b-a9806088c8ad', 'admin', 0, NULL, NULL, '01929300', NULL, NULL, 0, '2016-11-02 15:59:04', -33.72434, -15.99609, 1, 'admin@gmail.com', 123456, NULL, NULL, 'South Atlantic Ocean', 1, 0, 0, 0, 0, 0, 0, NULL, '2016-11-02', 'Hour', NULL, NULL, '22:58:00', '22:59:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'She gone', 'tuihjuiu', 'admin', NULL),
(92, 'a868d906-91dd-4827-9296-8533ef37467b', 'admin', 0, NULL, NULL, '01929300', NULL, NULL, 0, '2016-11-02 16:06:01', -33.72434, -15.99609, 1, 'admin@gmail.com', 123456789, NULL, NULL, 'South Atlantic Ocean', 1, 0, 0, 0, 0, 0, 0, NULL, '2016-11-02', 'Hour', NULL, NULL, '22:58:00', '22:59:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'She gone', 'tuan ', 'admin', NULL),
(93, 'abf0e356-3f97-4ee3-a512-a3a478084667', 'administration ', 0, NULL, NULL, '01929300', NULL, NULL, 0, '2016-11-02 16:08:29', -33.72434, -15.99609, 1, 'admin@gmail.com', 123456789, NULL, NULL, 'South Atlantic Ocean', 1, 0, 0, 0, 0, 0, 0, NULL, '2016-11-02', 'Hour', NULL, NULL, '00:08:00', '23:07:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'tuan ', 'admin', NULL),
(94, 'f182f3f3-15df-454b-bde6-d11a0611ecf6', 'admin', 0, NULL, NULL, '01929300', NULL, NULL, 0, '2016-11-03 06:06:04', -33.72434, -15.99609, 1, 'admin@gmail.com', 123, NULL, NULL, 'South Atlantic Ocean', 1, 0, 0, 0, 0, 0, 0, NULL, '2016-11-03', 'Hour', NULL, NULL, '13:03:00', '13:05:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'She gone', 'tuan ', 'admin', NULL),
(95, '31844477-3d1d-4dcb-a695-6aa0760b107c', 'admin', 0, NULL, NULL, '01929300', NULL, NULL, 0, '2016-11-03 06:18:11', -33.72434, -15.99609, 3, 'admin@gmail.com', 12, NULL, NULL, 'South Atlantic Ocean', 1, 0, 0, 0, 0, 0, 0, NULL, '2016-11-03', 'Hour', NULL, NULL, '13:03:00', '13:05:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'She gone', 'why', 'admin', NULL),
(96, '7bb68426-a52f-4fe7-9142-0e76b16b8f2a', 'admin', 0, NULL, NULL, '01929300', NULL, NULL, 0, '2016-11-03 07:31:09', -33.72434, -15.99609, 3, 'admin@gmail.com', 123, NULL, NULL, 'South Atlantic Ocean', 1, 1, 1, 1, 1, 1, 1, NULL, NULL, 'Hour', NULL, NULL, '14:29:00', '14:31:00', '14:29:00', '14:31:00', '14:29:00', NULL, '14:29:00', '14:31:00', '14:29:00', '14:31:00', '14:29:00', '14:31:00', '14:29:00', '14:31:00', NULL, 'tuan ', 'admin', NULL),
(97, 'c0b988bd-0381-4259-b036-d5bb8120f74d', 'admin', 0, NULL, NULL, '01929300', NULL, NULL, 0, '2016-11-03 08:11:02', -33.72434, -15.99609, 3, 'admin@gmail.com', NULL, NULL, NULL, 'South Atlantic Ocean', 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'tuan ', 'admin', NULL),
(98, '5b782208-f69c-40dd-b60d-22bcbbcc256d', 'quickjob001', 1, NULL, NULL, '0208080008', NULL, NULL, 0, '2016-11-16 08:15:59', 43.65444, -79.3807, 3, 'g@mail.com', 50, NULL, NULL, '220 Yonge St, Toronto, ON M5B 2H1, Canada', 1, 1, 1, 1, 1, 1, 1, NULL, '2016-11-16', 'Hour', NULL, NULL, '03:20:00', '21:43:00', '03:20:00', '21:43:00', '03:20:00', '21:43:00', '03:20:00', '21:43:00', '03:20:00', '21:43:00', '03:20:00', '21:43:00', '03:20:00', '21:43:00', 'good photographer', 'Abc photo', 'quickjob001', NULL),
(99, '27ccc2ab-1f39-4577-9346-ced7b520476c', 'quickjob001', 1, NULL, NULL, '0208080008', NULL, NULL, 0, '2016-11-16 08:16:01', 43.65444, -79.3807, 3, 'g@mail.com', 50, NULL, NULL, '220 Yonge St, Toronto, ON M5B 2H1, Canada', 1, 1, 1, 1, 1, 1, 1, NULL, '2016-11-16', 'Hour', NULL, NULL, '03:20:00', '21:43:00', '03:20:00', '21:43:00', '03:20:00', '21:43:00', '03:20:00', '21:43:00', '03:20:00', '21:43:00', '03:20:00', '21:43:00', '03:20:00', '21:43:00', 'good photographer', 'Abc photo', 'quickjob001', NULL),
(100, 'b2227166-1edd-4398-9086-b0baa7881b22', 'quickjob002', 0, NULL, NULL, '0208080008', NULL, NULL, 0, '2016-11-16 08:14:19', 43.65444, -79.3807, 3, 'g@mail.com', 50, NULL, NULL, '220 Yonge St, Toronto, ON M5B 2H1, Canada', 1, 1, 1, 1, 1, 1, 1, NULL, '2016-11-16', 'Hour', NULL, NULL, '03:20:00', '21:43:00', '03:20:00', '21:43:00', '03:20:00', '21:43:00', '03:20:00', '21:43:00', '03:20:00', '21:43:00', '03:20:00', '21:43:00', '03:20:00', '21:43:00', 'good photographer', 'Abc photo', 'quickjob001', NULL),
(101, '15eca0df-a723-4f4a-9955-892a8bbe34cd', 'quickjob002', 0, NULL, NULL, '0208080008', NULL, NULL, 0, '2016-11-16 08:15:40', 43.65444, -79.3807, 3, 'g@mail.com', 50, NULL, NULL, '220 Yonge St, Toronto, ON M5B 2H1, Canada', 1, 1, 1, 1, 1, 1, 1, NULL, '2016-11-16', 'Hour', NULL, NULL, '03:20:00', '21:43:00', '03:20:00', '21:43:00', '03:20:00', '21:43:00', '03:20:00', '21:43:00', '03:20:00', '21:43:00', '03:20:00', '21:43:00', '03:20:00', '21:43:00', 'good photographer', 'Abc photo', 'quickjob001', NULL),
(102, 'd72986f2-9fee-4b76-82f2-f0aac4c8b34c', 'quickjob001', 0, NULL, NULL, '0208080008', NULL, NULL, 0, '2016-11-17 14:21:11', 43.65444, -79.3807, 3, 'g@mail.com', 50, NULL, NULL, '220 Yonge St, Toronto, ON M5B 2H1, Canada', 1, 0, 0, 0, 0, 0, 0, NULL, '2016-11-17', 'Hour', NULL, NULL, '09:20:00', '10:20:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'good', 'land', 'quickjob001', NULL),
(103, 'da1326bb-b581-423e-9b7c-c77fcb8ccd9d', 'quickjob001', 0, NULL, NULL, '0208080008', NULL, NULL, 0, '2016-11-17 14:21:51', 43.65444, -79.3807, 3, 'g@mail.com', 50, NULL, NULL, '220 Yonge St, Toronto, ON M5B 2H1, Canada', 1, 0, 0, 0, 0, 0, 0, NULL, '2016-11-17', 'Hour', NULL, NULL, '09:20:00', '10:20:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'good', 'land', 'quickjob001', NULL),
(104, 'f23e5502-ded1-4695-95ad-bbb4320b0798', 'quickjob001', 0, NULL, NULL, '0208080008', NULL, NULL, 0, '2016-11-17 14:21:53', 43.65444, -79.3807, 3, 'g@mail.com', 50, NULL, NULL, '220 Yonge St, Toronto, ON M5B 2H1, Canada', 1, 0, 0, 0, 0, 0, 0, NULL, '2016-11-17', 'Hour', NULL, NULL, '09:20:00', '10:20:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'good', 'land', 'quickjob001', NULL),
(105, 'fdc4386c-85b8-431f-8ce3-b6a4bcbdf1f6', 'admin', 0, NULL, NULL, '01929300', NULL, NULL, 0, '2016-11-22 08:46:00', -33.72434, -15.99609, 3, 'admin@gmail.com', 122, NULL, NULL, 'South Atlantic Ocean', 1, 0, 0, 0, 0, 0, 0, NULL, '2016-11-22', 'Hour', NULL, NULL, '16:45:00', '15:44:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Tuan the y ', 'tuan ', 'admin', NULL),
(106, '150d3d95-7f24-4503-860f-494493130e2e', 'admin', 0, NULL, NULL, '01929300', NULL, NULL, 0, '2016-11-22 08:47:41', -33.72434, -15.99609, 3, 'admin@gmail.com', 123, NULL, NULL, 'South Atlantic Ocean', 1, 0, 0, 0, 0, 0, 0, NULL, '2016-11-22', 'Hour', NULL, NULL, '15:46:00', '15:51:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Tuan ', 'check notify error wrong', 'admin', NULL),
(107, 'f9479162-cb60-4ca1-9dd4-89baeda695c0', 'admin', 0, NULL, NULL, '01929300', NULL, NULL, 0, '2016-11-22 08:47:52', -33.72434, -15.99609, 3, 'admin@gmail.com', 123, NULL, NULL, 'South Atlantic Ocean', 1, 0, 0, 0, 0, 0, 0, NULL, '2016-11-22', 'Hour', NULL, NULL, '15:46:00', '15:51:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Tuan ', 'check notify error wrong', 'admin', NULL);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

-- phpMyAdmin SQL Dump
-- version 3.5.8.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 22, 2016 at 09:28 AM
-- Server version: 5.5.31
-- PHP Version: 5.3.29

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `quickjob_v2`
--

-- --------------------------------------------------------

--
-- Table structure for table `emp_skill`
--

CREATE TABLE IF NOT EXISTS `emp_skill` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` varchar(100) NOT NULL,
  `skill_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `1_idx` (`employee_id`),
  KEY `emp_skil_idx` (`employee_id`),
  KEY `skill_skill_idx` (`skill_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=71 ;

--
-- Dumping data for table `emp_skill`
--

INSERT INTO `emp_skill` (`id`, `employee_id`, `skill_id`) VALUES
(1, 'admin', 3),
(2, 'admin2', 2),
(3, 'admin', 1),
(6, 'test1241', 2),
(7, 'test1241', 1),
(10, 'tester', 2),
(11, 'tester', 1),
(12, 'test124142', 2),
(13, 'test124142', 1),
(14, 'test12345', 1),
(15, '12', 3),
(16, '123', 3),
(17, 'Trung DZ', 1),
(18, 'anhdt12', 2),
(19, 'anhdt12', 1),
(20, 'TrungVKL', 2),
(21, 'long1234', 2),
(22, 'nam1234', 1),
(23, 'long12', 1),
(24, 'tuanvu2605', 1),
(25, 'long999', 1),
(26, '8fa3a85e-0e12-40d8-9846-a2f7cb7f8a31', 2),
(27, 'cbf05259-1163-44a3-9fb5-adc0a5d7a8e0', 1),
(28, 'afd53882-502b-48f7-a430-0dae1a55e820', 5),
(29, '3de4dabc-cbd5-4315-a3cb-ef86188043bc', 4),
(30, '80c8bbad-91a1-4e4c-8f40-b2ff14a5f307', 5),
(31, '0088a760-d31f-4039-8ec5-e7db3eb6544a', 5),
(32, '81032f63-2db0-4943-a0f4-ae125f02504d', 21),
(33, '1c83781e-9810-475d-9ee4-53200197d666', 3),
(34, 'a8027e0b-37c6-4dfd-8f91-b1a194d99c04', 5),
(35, 'a3d15434-9f25-414b-9de0-c39ecc1d0d0f', 6),
(36, '922e4479-e507-433d-b6ca-106c0d25e47a', 5),
(37, 'e177a2d1-1df2-4ba5-a959-634eef527591', 3),
(38, '95b26d8e-f4a8-4c02-ac9a-9e425a05872e', 6),
(39, '9b274c8e-c46b-41bc-840f-8b58a3e378fd', 1),
(40, '8f6ef276-02de-4d30-a9cd-a634824be018', 1),
(41, '4c341d0e-2c26-4b46-bb24-767fb4de8897', 1),
(42, '151bee69-ed06-47cc-a6af-51152348a5b9', 3),
(43, 'd7c65da7-135f-4f65-8101-8612690f4f63', 8),
(44, 'e6f041f9-abfe-40f0-be61-a7c6eebbbeac', 2),
(45, 'b1a9ea50-cc80-4851-ac55-21bc235ea5e5', 2),
(46, 'e30d7dc0-056c-4d3a-bcaa-482a370fbf00', 2),
(47, '8d6fea7c-21e2-48f6-a169-eb6bcdeacef3', 5),
(48, '1c6bec10-79e5-49a0-bbed-55177e347e21', 5),
(49, '1c4c3516-39d4-4c05-b2e3-1e22e15557fb', 1),
(50, 'ae11d8fa-985c-4b97-8408-cb6466e99512', 1),
(51, 'ec5d06c6-40f2-490b-9647-c072723d7d70', 1),
(52, 'ede86a1a-b6cc-448a-b2fd-ee8002df4f56', 1),
(53, '60167236-62b0-4882-80b7-4befa2bb0057', 1),
(54, '770e24eb-bf8c-42e4-973b-a9806088c8ad', 1),
(55, 'a868d906-91dd-4827-9296-8533ef37467b', 1),
(56, 'abf0e356-3f97-4ee3-a512-a3a478084667', 1),
(57, 'f182f3f3-15df-454b-bde6-d11a0611ecf6', 1),
(58, '31844477-3d1d-4dcb-a695-6aa0760b107c', 1),
(59, '7bb68426-a52f-4fe7-9142-0e76b16b8f2a', 1),
(60, 'c0b988bd-0381-4259-b036-d5bb8120f74d', 1),
(61, '5b782208-f69c-40dd-b60d-22bcbbcc256d', 2),
(62, '27ccc2ab-1f39-4577-9346-ced7b520476c', 2),
(63, 'b2227166-1edd-4398-9086-b0baa7881b22', 2),
(64, '15eca0df-a723-4f4a-9955-892a8bbe34cd', 2),
(65, 'd72986f2-9fee-4b76-82f2-f0aac4c8b34c', 1),
(66, 'da1326bb-b581-423e-9b7c-c77fcb8ccd9d', 1),
(67, 'f23e5502-ded1-4695-95ad-bbb4320b0798', 1),
(68, 'fdc4386c-85b8-431f-8ce3-b6a4bcbdf1f6', 1),
(69, '150d3d95-7f24-4503-860f-494493130e2e', 1),
(70, 'f9479162-cb60-4ca1-9dd4-89baeda695c0', 1);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `emp_skill`
--
ALTER TABLE `emp_skill`
  ADD CONSTRAINT `emp_skil` FOREIGN KEY (`employee_id`) REFERENCES `employees` (`profile_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `skill_skill` FOREIGN KEY (`skill_id`) REFERENCES `skill_set` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

package vn.com.arillance.imagehub.model;

import java.sql.Date;
import java.util.ArrayList;

public class JobModel {
	private int availableFlag;

	private int id;
	private String jobName;
	private String jobDescription;
	private String requirement;
	private String phoneNumber;
	private int employeeType;
	private String startTime;
	private String endTime;
	private double salary;
	private boolean jobDone = false;
	private boolean wdMon;
	private boolean wdTue;
	private boolean wdWed;
	private boolean wdThi;
	private boolean wdFri;
	private boolean wdSat;
	private boolean wdSun;
	private ArrayList<Integer> skills;
	private String jobId;
	private String userId;
	private int deleteFlag;
	private Double latitude;
	private Double longitude;
	private Date startDate;
	private Date endDate;
	private String city;
	private String salaryUnit;
	private String monStartTime;
	private String monEndTime;
	private String tueStartTime;
	private String tueEndTime;
	private String wedStartTime;
	private String wedEndTime;
	private String thiStartTime;
	private String thiEndTime;
	private String friStartTime;
	private String friEndTime;
	private String satStartTime;
	private String satEndTime;
	private String sunStartTime;
	private String sunEndTime;
	private double distance;
	
	public String getJobName() {
		return jobName;
	}

	public void setJobName(String jobName) {
		this.jobName = jobName;
	}

	public String getJobDescription() {
		return jobDescription;
	}

	public void setJobDescription(String jobDescription) {
		this.jobDescription = jobDescription;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public int getEmployeeType() {
		return employeeType;
	}

	public void setEmployeeType(int employeeType) {
		this.employeeType = employeeType;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public double getSalary() {
		return salary;
	}

	public void setSalary(double salary) {
		this.salary = salary;
	}

	public boolean isJobDone() {
		return jobDone;
	}

	public void setJobDone(boolean jobDone) {
		this.jobDone = jobDone;
	}

	public boolean isWdMon() {
		return wdMon;
	}

	public void setWdMon(boolean wdMon) {
		this.wdMon = wdMon;
	}

	public boolean isWdTue() {
		return wdTue;
	}

	public void setWdTue(boolean wdTue) {
		this.wdTue = wdTue;
	}

	public boolean isWdWed() {
		return wdWed;
	}

	public void setWdWed(boolean wdWed) {
		this.wdWed = wdWed;
	}

	public boolean isWdThi() {
		return wdThi;
	}

	public void setWdThi(boolean wdThi) {
		this.wdThi = wdThi;
	}

	public boolean isWdFri() {
		return wdFri;
	}

	public void setWdFri(boolean wdFri) {
		this.wdFri = wdFri;
	}

	public boolean isWdSat() {
		return wdSat;
	}

	public void setWdSat(boolean wdSat) {
		this.wdSat = wdSat;
	}

	public boolean isWdSun() {
		return wdSun;
	}

	public void setWdSun(boolean wdSun) {
		this.wdSun = wdSun;
	}

	public ArrayList<Integer> getSkills() {
		return skills;
	}

	public void setSkills(ArrayList<Integer> skills) {
		this.skills = skills;
	}

	public String getJobId() {
		return jobId;
	}

	public void setJobId(String jobId) {
		this.jobId = jobId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public int getDeleteFlag() {
		return deleteFlag;
	}

	public void setDeleteFlag(int deleteFlag) {
		this.deleteFlag = deleteFlag;
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getSalaryUnit() {
		return salaryUnit;
	}

	public void setSalaryUnit(String salaryUnit) {
		this.salaryUnit = salaryUnit;
	}

	public String getMonStartTime() {
		return monStartTime;
	}

	public void setMonStartTime(String monStartTime) {
		this.monStartTime = monStartTime;
	}

	public String getMonEndTime() {
		return monEndTime;
	}

	public void setMonEndTime(String monEndTime) {
		this.monEndTime = monEndTime;
	}

	public String getTueStartTime() {
		return tueStartTime;
	}

	public void setTueStartTime(String tueStartTime) {
		this.tueStartTime = tueStartTime;
	}

	public String getTueEndTime() {
		return tueEndTime;
	}

	public void setTueEndTime(String tueEndTime) {
		this.tueEndTime = tueEndTime;
	}

	public String getWedStartTime() {
		return wedStartTime;
	}

	public void setWedStartTime(String wedStartTime) {
		this.wedStartTime = wedStartTime;
	}

	public String getWedEndTime() {
		return wedEndTime;
	}

	public void setWedEndTime(String wedEndTime) {
		this.wedEndTime = wedEndTime;
	}

	public String getThiStartTime() {
		return thiStartTime;
	}

	public void setThiStartTime(String thiStartTime) {
		this.thiStartTime = thiStartTime;
	}

	public String getThiEndTime() {
		return thiEndTime;
	}

	public void setThiEndTime(String thiEndTime) {
		this.thiEndTime = thiEndTime;
	}

	public String getFriStartTime() {
		return friStartTime;
	}

	public void setFriStartTime(String friStartTime) {
		this.friStartTime = friStartTime;
	}

	public String getFriEndTime() {
		return friEndTime;
	}

	public void setFriEndTime(String friEndTime) {
		this.friEndTime = friEndTime;
	}

	public String getSatStartTime() {
		return satStartTime;
	}

	public void setSatStartTime(String satStartTime) {
		this.satStartTime = satStartTime;
	}

	public String getSatEndTime() {
		return satEndTime;
	}

	public void setSatEndTime(String satEndTime) {
		this.satEndTime = satEndTime;
	}

	public String getSunStartTime() {
		return sunStartTime;
	}

	public void setSunStartTime(String sunStartTime) {
		this.sunStartTime = sunStartTime;
	}

	public String getSunEndTime() {
		return sunEndTime;
	}

	public void setSunEndTime(String sunEndTime) {
		this.sunEndTime = sunEndTime;
	}

	public String getRequirement() {
		return requirement;
	}

	public void setRequirement(String requirement) {
		this.requirement = requirement;
	}

	public void calRange() {
		
	}
	

	
	public double getDistance() {
		return distance;
	}

	public void setDistance(double distance) {
		this.distance = distance;
	}

	public int getAvailableFlag() {
		return availableFlag;
	}

	public void setAvailableFlag(int availableFlag) {
		this.availableFlag = availableFlag;
	}
}

package vn.com.arillance.imagehub.model;

public class UserModel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2421326185343338957L;
	private int id;
	private String userId;
	private String token;
	private String socialType;
	private String name;
	private String password;
	private String oldPassword;
	private String notiToken;

	private boolean availableFlag;
	private String imgUrl = "";

	private String experience;
	private boolean deleteFlag;

	private String email;

	private String address;

	private String phoneNumber;

	private String skillDesc;
	private String description;

	public UserModel() {
		super();
	}

	public UserModel(String userId, String name, String password, boolean availableFlag, String imgUrl,
			String experience, boolean deleteFlag, String email, String address, String phoneNumber, String skillDesc,
			String description) {
		super();
		this.userId = userId;
		this.name = name;
		this.password = password;
		this.availableFlag = availableFlag;
		this.imgUrl = imgUrl;
		this.experience = experience;
		this.deleteFlag = deleteFlag;
		this.email = email;
		this.address = address;
		this.phoneNumber = phoneNumber;
		this.skillDesc = skillDesc;
		this.description = description;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public boolean isAvailableFlag() {
		return availableFlag;
	}

	public void setAvailableFlag(boolean availableFlag) {
		this.availableFlag = availableFlag;
	}

	public String getImgUrl() {
		return imgUrl;
	}

	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}

	public String getExperience() {
		return experience;
	}

	public void setExperience(String experience) {
		this.experience = experience;
	}

	public boolean isDeleteFlag() {
		return deleteFlag;
	}

	public void setDeleteFlag(boolean deleteFlag) {
		this.deleteFlag = deleteFlag;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSkillDesc() {
		return skillDesc;
	}

	public void setSkillDesc(String skillDesc) {
		this.skillDesc = skillDesc;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getSocialType() {
		return socialType;
	}

	public void setSocialType(String socialType) {
		this.socialType = socialType;
	}

	public String getOldPassword() {
		return oldPassword;
	}

	public void setOldPassword(String oldPassword) {
		this.oldPassword = oldPassword;
	}

	public String getNotiToken() {
		return notiToken;
	}

	public void setNotiToken(String notiToken) {
		this.notiToken = notiToken;
	}

}

package vn.com.arillance.imagehub.model;

import java.io.Serializable;

public class AuthModel implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -2707256519400126734L;
	private String userId;
	private String password;

	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
}

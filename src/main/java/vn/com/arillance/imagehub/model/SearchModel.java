package vn.com.arillance.imagehub.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

public class SearchModel implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8128069764987055992L;
	private String userId;
	private String phoneNumber;
	private ArrayList<Integer> skills;
	private int employeeType;
	private ArrayList<Weekday> weekdays;
	private String startTime;
	private String endTime;
	private PositionModel position;
	private double salaryFrom;
	private double salaryTo;
	private int availableFlag = -1;
	private String password;
	private Date startDate;
	private Date endDate;
	private String salaryUnit;
	private String jobId;
	private String profileId;
	private String columnSort;
	private String orderSort;
	private PositionModel curPosition;
	private ArrayList<SalaryModel> salaryList;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public ArrayList<Integer> getSkills() {
		return skills;
	}

	public void setSkills(ArrayList<Integer> skills) {
		this.skills = skills;
	}

	public int getEmployeeType() {
		return employeeType;
	}

	public void setEmployeeType(int employeeType) {
		this.employeeType = employeeType;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public double getSalaryFrom() {
		return salaryFrom;
	}

	public void setSalaryFrom(double salaryFrom) {
		this.salaryFrom = salaryFrom;
	}

	public double getSalaryTo() {
		return salaryTo;
	}

	public void setSalaryTo(double salaryTo) {
		this.salaryTo = salaryTo;
	}

	public int getAvailableFlag() {
		return availableFlag;
	}

	public void setAvailableFlag(int availableFlag) {
		this.availableFlag = availableFlag;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public PositionModel getPosition() {
		return position;
	}

	public void setPosition(PositionModel position) {
		this.position = position;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public String getSalaryUnit() {
		return salaryUnit;
	}

	public void setSalaryUnit(String salaryUnit) {
		this.salaryUnit = salaryUnit;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public ArrayList<Weekday> getWeekdays() {
		return weekdays;
	}

	public void setWeekdays(ArrayList<Weekday> weekdays) {
		this.weekdays = weekdays;
	}

	public String getJobId() {
		return jobId;
	}

	public void setJobId(String jobId) {
		this.jobId = jobId;
	}

	public String getProfileId() {
		return profileId;
	}

	public void setProfileId(String profileId) {
		this.profileId = profileId;
	}

	public String getColumnSort() {
		return columnSort;
	}

	public void setColumnSort(String columnSort) {
		this.columnSort = columnSort;
	}

	public String getOrderSort() {
		return orderSort;
	}

	public void setOrderSort(String orderSort) {
		this.orderSort = orderSort;
	}

	public PositionModel getCurPosition() {
		return curPosition;
	}

	public void setCurPosition(PositionModel curPosition) {
		this.curPosition = curPosition;
	}

	public ArrayList<SalaryModel> getSalaryList() {
		return salaryList;
	}

	public void setSalaryList(ArrayList<SalaryModel> salaryList) {
		this.salaryList = salaryList;
	}
	
}

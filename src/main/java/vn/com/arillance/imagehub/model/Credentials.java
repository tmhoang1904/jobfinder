package vn.com.arillance.imagehub.model;

import java.io.Serializable;

public class Credentials implements Serializable {
	private String fbId;
	private String accessToken;

	public String getFbId() {
		return fbId;
	}

	public void setFbId(String fbId) {
		this.fbId = fbId;
	}

	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}
}

package vn.com.arillance.imagehub.model;

import java.io.Serializable;

public class SalaryModel implements Serializable {
	private double salaryFrom;
	private double salaryTo;
	private String salaryUnit;
	public double getSalaryFrom() {
		return salaryFrom;
	}
	public void setSalaryFrom(double salaryFrom) {
		this.salaryFrom = salaryFrom;
	}
	public double getSalaryTo() {
		return salaryTo;
	}
	public void setSalaryTo(double salaryTo) {
		this.salaryTo = salaryTo;
	}
	public String getSalaryUnit() {
		return salaryUnit;
	}
	public void setSalaryUnit(String salaryUnit) {
		this.salaryUnit = salaryUnit;
	}
	
}

package vn.com.arillance.imagehub.model;

public class PositionModel {
	private double latitude = 0;
	private double longitude = 0;
	private double range = 20;

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public double getRange() {
		return range;
	}

	public void setRange(double range) {
		this.range = range;
	}

	public double getNorthLatitude() {
		return latitude + (range / 6378) * (180 / Math.PI);
	}

	public double getSouthLatitude() {
		return latitude - (range / 6378) * (180 / Math.PI);
	}

	public double getEastLongitude() {
		return longitude + (range / 6378) * (180 / Math.PI)
				/ Math.cos(latitude * Math.PI / 180);
	}

	public double getWestLongitude() {
		return longitude - (range / 6378) * (180 / Math.PI)
				/ Math.cos(latitude * Math.PI / 180);
	}
}

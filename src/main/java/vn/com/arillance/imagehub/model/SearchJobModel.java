package vn.com.arillance.imagehub.model;

import java.sql.Date;
import java.util.ArrayList;

public class SearchJobModel {
	private String jobName;
	private String jobId;
	private String phoneNumber;
	private ArrayList<Integer> skills;
	private int employeeType;
	private ArrayList<Weekday> weekdays;
	private String startTime;
	private String endTime;
	private PositionModel position;	
	private int jobDone = -1;
	private Date startDate;
	private Date endDate;
	private String city;
	private String salaryUnit;
	private double salaryFrom;
	private double salaryTo;
	private ArrayList<SalaryModel> salaryList;
	private ArrayList<Order> orderSort;
	private PositionModel curPosition;
	
	public String getJobName() {
		return jobName;
	}

	public void setJobName(String jobName) {
		this.jobName = jobName;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public ArrayList<Integer> getSkills() {
		return skills;
	}

	public void setSkills(ArrayList<Integer> skills) {
		this.skills = skills;
	}

	public int getEmployeeType() {
		return employeeType;
	}

	public void setEmployeeType(int employeeType) {
		this.employeeType = employeeType;
	}


	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}


//
	public double getSalaryFrom() {
		return salaryFrom;
	}

	public void setSalaryFrom(double salaryFrom) {
		this.salaryFrom = salaryFrom;
	}

	public double getSalaryTo() {
		return salaryTo;
	}

	public void setSalaryTo(double salaryTo) {
		this.salaryTo = salaryTo;
	}

	public int getJobDone() {
		return jobDone;
	}

	public void setJobDone(int jobDone) {
		this.jobDone = jobDone;
	}

	public String getJobId() {
		return jobId;
	}

	public void setJobId(String jobId) {
		this.jobId = jobId;
	}

	public PositionModel getPosition() {
		return position;
	}

	public void setPosition(PositionModel position) {
		this.position = position;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getSalaryUnit() {
		return salaryUnit;
	}

	public void setSalaryUnit(String salaryUnit) {
		this.salaryUnit = salaryUnit;
	}

	public ArrayList<Weekday> getWeekdays() {
		return weekdays;
	}

	public void setWeekdays(ArrayList<Weekday> weekdays) {
		this.weekdays = weekdays;
	}

	

	public PositionModel getCurPosition() {
		return curPosition;
	}

	public void setCurPosition(PositionModel curPosition) {
		this.curPosition = curPosition;
	}

	public ArrayList<Order> getOrderSort() {
		return orderSort;
	}

	public void setOrderSort(ArrayList<Order> orderSort) {
		this.orderSort = orderSort;
	}

	public ArrayList<SalaryModel> getSalaryList() {
		return salaryList;
	}

	public void setSalaryList(ArrayList<SalaryModel> salaryList) {
		this.salaryList = salaryList;
	}

	
}

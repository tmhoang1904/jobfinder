package vn.com.arillance.imagehub.model;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.validation.Valid;
import javax.validation.constraints.Size;

public class PostModel {
	private int postId;
	@Valid
	@Size(min = 10, max = 150)
	private String postTitle;
	private String postContent;
	private String imageUrl;
	private int viewCount;
	private Date createDate;
	private int deleteFlag;
	private int enableFlag;
	private CategoryModel category;
	private ProfileModel author;
	private String gifThumb;
	public int getPostId() {
		return postId;
	}

	public void setPostId(int postId) {
		this.postId = postId;
	}

	public String getPostTitle() {
		return postTitle;
	}

	public void setPostTitle(String postTitle) {
		this.postTitle = postTitle;
	}

	public String getPostContent() {
		return postContent;
	}

	public void setPostContent(String postContent) {
		this.postContent = postContent;
	}

	public int getViewCount() {
		return viewCount;
	}

	public void setViewCount(int viewCount) {
		this.viewCount = viewCount;
	}

	public String getCreateDate() {
		String newDateString = new SimpleDateFormat("dd MMM yyyy")
				.format(createDate);
		return newDateString;
	}

	public Date getCreateDateDb() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public int getDeleteFlag() {
		return deleteFlag;
	}

	public void setDeleteFlag(int deleteFlag) {
		this.deleteFlag = deleteFlag;
	}

	public int getEnableFlag() {
		return enableFlag;
	}

	public void setEnableFlag(int enableFlag) {
		this.enableFlag = enableFlag;
	}

	public CategoryModel getCategory() {
		return category;
	}

	public void setCategory(CategoryModel category) {
		this.category = category;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public ProfileModel getAuthor() {
		return author;
	}

	public void setAuthor(ProfileModel author) {
		this.author = author;
	}

	public String getGifThumb() {
		return imageUrl.split(".")[0]+".png";
	}

	public void setGifThumb(String gifThumb) {
		this.gifThumb = gifThumb;
	}
	

}

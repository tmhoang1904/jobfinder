package vn.com.arillance.imagehub.model;

import java.sql.Date;
import java.util.ArrayList;

public class ProfileModel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2421326185343338957L;
	private int id;
	private String userId;
	private String name;
	private String password;
	private boolean availableFlag;
	private String imgUrl;
	private String startTime;
	private String endTime;
	private String experience;
	private boolean deleteFlag;
	private Double latitude;
	private Double longitude;
	private int employeeType;
	private String email;
	private Double salary;
	private Double latitudeAddress;
	private Double longitudeAddres;
	private String address;
	private boolean wdMon;
	private boolean wdTue;
	private boolean wdWed;
	private boolean wdThi;
	private boolean wdFri;
	private boolean wdSat;
	private boolean wdSun;
	private String phoneNumber;
	private ArrayList<Integer> skills;
	private Date startDate;
	private Date endDate;
	private String notiToken;

	private String salaryUnit;
	private String description;
	private String monStartTime;
	private String monEndTime;
	private String tueStartTime;
	private String tueEndTime;
	private String wedStartTime;
	private String wedEndTime;
	private String thiStartTime;
	private String thiEndTime;
	private String friStartTime;
	private String friEndTime;
	private String satStartTime;
	private String satEndTime;
	private String sunStartTime;
	private String sunEndTime;
	private String skillDesc;
	private String profileName;
	private String profileId;
	private String distance;
	private String portfolio;
	
	
	public String getPortfolio() {
		return portfolio;
	}

	public void setPortfolio(String portfolio) {
		this.portfolio = portfolio;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public boolean isAvailableFlag() {
		return availableFlag;
	}

	public void setAvailableFlag(boolean availableFlag) {
		this.availableFlag = availableFlag;
	}

	public String getImgUrl() {
		return imgUrl;
	}

	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public String getExperience() {
		return experience;
	}

	public void setExperience(String experience) {
		this.experience = experience;
	}

	public boolean isDeleteFlag() {
		return deleteFlag;
	}

	public void setDeleteFlag(boolean deleteFlag) {
		this.deleteFlag = deleteFlag;
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public int getEmployeeType() {
		return employeeType;
	}

	public void setEmployeeType(int employeeType) {
		this.employeeType = employeeType;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Double getSalary() {
		return salary;
	}

	public void setSalary(Double salary) {
		this.salary = salary;
	}

	public Double getLatitudeAddress() {
		return latitudeAddress;
	}

	public void setLatitudeAddress(Double latitudeAddress) {
		this.latitudeAddress = latitudeAddress;
	}

	public Double getLongitudeAddres() {
		return longitudeAddres;
	}

	public void setLongitudeAddres(Double longitudeAddres) {
		this.longitudeAddres = longitudeAddres;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public boolean isWdMon() {
		return wdMon;
	}

	public void setWdMon(boolean wdMon) {
		this.wdMon = wdMon;
	}

	public boolean isWdTue() {
		return wdTue;
	}

	public void setWdTue(boolean wdTue) {
		this.wdTue = wdTue;
	}

	public boolean isWdWed() {
		return wdWed;
	}

	public void setWdWed(boolean wdWed) {
		this.wdWed = wdWed;
	}

	public boolean isWdThi() {
		return wdThi;
	}

	public void setWdThi(boolean wdThi) {
		this.wdThi = wdThi;
	}

	public boolean isWdFri() {
		return wdFri;
	}

	public void setWdFri(boolean wdFri) {
		this.wdFri = wdFri;
	}

	public boolean isWdSat() {
		return wdSat;
	}

	public void setWdSat(boolean wdSat) {
		this.wdSat = wdSat;
	}

	public boolean isWdSun() {
		return wdSun;
	}

	public void setWdSun(boolean wdSun) {
		this.wdSun = wdSun;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public ArrayList<Integer> getSkills() {
		return skills;
	}

	public void setSkills(ArrayList<Integer> skills) {
		this.skills = skills;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public String getSalaryUnit() {
		return salaryUnit;
	}

	public void setSalaryUnit(String salaryUnit) {
		this.salaryUnit = salaryUnit;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getMonStartTime() {
		return monStartTime;
	}

	public void setMonStartTime(String monStartTime) {
		this.monStartTime = monStartTime;
	}

	public String getMonEndTime() {
		return monEndTime;
	}

	public void setMonEndTime(String monEndTime) {
		this.monEndTime = monEndTime;
	}

	public String getTueStartTime() {
		return tueStartTime;
	}

	public void setTueStartTime(String tueStartTime) {
		this.tueStartTime = tueStartTime;
	}

	public String getTueEndTime() {
		return tueEndTime;
	}

	public void setTueEndTime(String tueEndTime) {
		this.tueEndTime = tueEndTime;
	}

	public String getWedStartTime() {
		return wedStartTime;
	}

	public void setWedStartTime(String wedStartTime) {
		this.wedStartTime = wedStartTime;
	}

	public String getWedEndTime() {
		return wedEndTime;
	}

	public void setWedEndTime(String wedEndTime) {
		this.wedEndTime = wedEndTime;
	}

	public String getThiStartTime() {
		return thiStartTime;
	}

	public void setThiStartTime(String thiStartTime) {
		this.thiStartTime = thiStartTime;
	}

	public String getThiEndTime() {
		return thiEndTime;
	}

	public void setThiEndTime(String thiEndTime) {
		this.thiEndTime = thiEndTime;
	}

	public String getFriStartTime() {
		return friStartTime;
	}

	public void setFriStartTime(String friStartTime) {
		this.friStartTime = friStartTime;
	}

	public String getFriEndTime() {
		return friEndTime;
	}

	public void setFriEndTime(String friEndTime) {
		this.friEndTime = friEndTime;
	}

	public String getSatStartTime() {
		return satStartTime;
	}

	public void setSatStartTime(String satStartTime) {
		this.satStartTime = satStartTime;
	}

	public String getSatEndTime() {
		return satEndTime;
	}

	public void setSatEndTime(String satEndTime) {
		this.satEndTime = satEndTime;
	}

	public String getSunStartTime() {
		return sunStartTime;
	}

	public void setSunStartTime(String sunStartTime) {
		this.sunStartTime = sunStartTime;
	}

	public String getSunEndTime() {
		return sunEndTime;
	}

	public void setSunEndTime(String sunEndTime) {
		this.sunEndTime = sunEndTime;
	}

	public String getSkillDesc() {
		return skillDesc;
	}

	public void setSkillDesc(String skillDesc) {
		this.skillDesc = skillDesc;
	}

	public String getProfileName() {
		return profileName;
	}

	public void setProfileName(String profileName) {
		this.profileName = profileName;
	}

	public String getProfileId() {
		return profileId;
	}

	public void setProfileId(String profileId) {
		this.profileId = profileId;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getDistance() {
		return distance;
	}

	public void setDistance(String distance) {
		this.distance = distance;
	}

	public String getNotiToken() {
		return notiToken;
	}

	public void setNotiToken(String notiToken) {
		this.notiToken = notiToken;
	}
	

}

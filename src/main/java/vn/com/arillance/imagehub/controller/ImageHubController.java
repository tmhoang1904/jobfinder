//package vn.com.arillance.imagehub.controller;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.ws.rs.GET;
//import javax.ws.rs.Path;
//import javax.ws.rs.core.Context;
//
//import org.apache.log4j.Logger;
//import org.springframework.security.access.annotation.Secured;
//import org.springframework.stereotype.Component;
//
//@Component
//@Secured("ROLE_USER")
//@Path("/api")
//public class ImageHubController {
//	@GET
//	@Path("/world")
//	public String test() {
//		return "Hello world!";
//	}
//
//	static Logger log = Logger.getLogger(ImageHubController.class.getName());
//	@Context
//	private HttpServletRequest servletRequest;
//}

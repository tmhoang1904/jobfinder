package vn.com.arillance.imagehub.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URLEncoder;

import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;

public class HTTPUtils {
	public static final int NEW_ORDER = 1;
	public static final int NEW_ORDER_CONFIRM = 2;
	public static final int WEEKLY_SCHEDULE = 3;
	private static final String USER_AGENT = "Mozilla/5.0";
//	public static final String NOTI_MESSAGE = "{ \"data\": { \"title\": \"%s\", \"content\": \"%s\", \"dataFrom\" : \"%s\" }, \"registration_ids\" : [\"%s\"]}";
//	public static final String NOTI_MESSAGE = "{ \"notification\":{\"title\" : \"%s\", \"body\":\"%s\"}, \"to\" : \"%s\"}";
	public static final String NOTI_MESSAGE = "{ \"notification\":{\"title\" : \"%s\", \"body\":\"%s\", \"icon\" : \"app_icon\", \"sound\" : \"default\"}, \"to\" : \"%s\"}";

	public static void pushNotficationToAllDevices(String rootToken, String title, String content, int dataFrom) {
		if (rootToken != null && !rootToken.trim().equals("")) {
			String[] tokens = rootToken.split(",");
			System.out.println("Noti Token : " + rootToken);
			for (String token : tokens) {
				System.out.println("Token : " + token);
				HTTPUtils.pushNotification(title, content, token.trim(), dataFrom);				
			}
		}
	}

	public static void pushNotification(String title, String content, String notiToken, int dataFrom) {
		try {
//			String body = "{\"notification\": {\"title\": \"%s\", \"body\": \"%s\"}, \"to\" : \"%s\"}";
			String body = NOTI_MESSAGE;
			body = String.format(body, title, content, notiToken);			
			CloseableHttpClient httpClient = HttpClientBuilder.create().build();
			// DefaultHttpClient httpClient = new DefaultHttpClient();
			HttpPost postRequest = new HttpPost("https://fcm.googleapis.com/fcm/send");
			StringEntity input = new StringEntity(body, "UTF-8");
			input.setContentType("application/json");
			postRequest.setHeader("Authorization", "key= AIzaSyB4OOdKro3eNJyjahj1lOHhkjCwF7R2t0M");
			postRequest.setHeader("Content-Type", "application/json");
			postRequest.setEntity(input);
			
			System.out.println("Request : " + body);

			for (Header header : postRequest.getAllHeaders()) {
				System.out.println("Header : " + header.toString());
			}
			HttpResponse response = httpClient.execute(postRequest);
			if (response.getStatusLine().getStatusCode() != 200) {
				System.out.println(response.toString());
				throw new RuntimeException("Failed : HTTP error code : " + response.getStatusLine().getStatusCode());
			}

			BufferedReader br = new BufferedReader(new InputStreamReader((response.getEntity().getContent())));

			String output;
			System.out.println("Output from Server .... \n");
			while ((output = br.readLine()) != null) {
				System.out.println(output);
			}

			httpClient.close();

		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
}

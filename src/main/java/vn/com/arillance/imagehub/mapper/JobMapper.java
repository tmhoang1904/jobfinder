package vn.com.arillance.imagehub.mapper;

import java.util.ArrayList;

import vn.com.arillance.imagehub.model.JobModel;
import vn.com.arillance.imagehub.model.JobTitle;
import vn.com.arillance.imagehub.model.SearchJobModel;
import vn.com.arillance.imagehub.model.SearchModel;
import vn.com.arillance.imagehub.model.SkillSet;

public interface JobMapper {

	ArrayList<JobModel> searchJob(SearchJobModel search);

	JobModel getJob(String jobId);

	void createJob(JobModel job);

	void createJobSkill(JobModel job);

	ArrayList<SkillSet> getSkillSet();

	int getMaxIdDb();

	ArrayList<String> getListCity();

	ArrayList<JobTitle> getListJob();

	void addTitle(ArrayList<String> listArray);

	ArrayList<JobModel> getListJobByUser(String userId);

	void updateFlag(SearchModel user);

	ArrayList<JobModel> getHistoryJob(String userid);
	
	int removeJob(JobModel job);

}

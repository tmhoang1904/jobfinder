package vn.com.arillance.imagehub.mapper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import vn.com.arillance.imagehub.model.JobModel;
import vn.com.arillance.imagehub.model.ProfileModel;
import vn.com.arillance.imagehub.model.SearchJobModel;
import vn.com.arillance.imagehub.model.SearchModel;
import vn.com.arillance.imagehub.model.SkillSet;
import vn.com.arillance.imagehub.model.UserModel;

public interface UserMapper {

	UserModel login(HashMap<String, String> mapParam);

	void register(UserModel user);

	void createProfile(ProfileModel profile);
	
	List<SkillSet> getAllSkillsEmp(String profileId);

	ArrayList<ProfileModel> searchUser(SearchModel search);

	ProfileModel getuser(String userId);

	ArrayList<JobModel> searchJob(SearchJobModel search);

	void createUserSkill(ProfileModel user);

	void updatePosition(SearchModel user);

	String checkEmail(String email);

	void updateAccount(UserModel user);

	void changePassword(UserModel user);

	ArrayList<ProfileModel> getListProfile(String userId);

	void updateFlag(SearchModel user);

	UserModel loginSocial(String userId);

	boolean getLoginSocial(String userId);

	int updateNotiToken(HashMap<String, Object> mapParam);

	String getNotiToken(int skillId);
	
	List<String> getNotiTokenFromJob(String jobId);

	int removeProfile(String profileId);
}

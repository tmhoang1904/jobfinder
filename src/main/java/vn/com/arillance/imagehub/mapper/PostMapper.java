package vn.com.arillance.imagehub.mapper;

import java.util.HashMap;
import java.util.List;

import vn.com.arillance.imagehub.model.PostModel;

public interface PostMapper {

	public List<PostModel> getAllPosts(HashMap<String, Object> mapParam);

	public void insertPost(PostModel postModel);

	public void updatePost(PostModel postModel);

	public void deletePost(Integer postId);

	public void enablePost(Integer postId);

	public PostModel getPostById(Integer postId);

	public void updateCount(Integer postId);

	//
	// public PostModel getPostByIdAdmin(Integer postId);
	//
	// public List<PostModel> getAllPosts(HashMap<String, Integer> mapParam);
	//
	// public List<PostModel> getAllPostsAdmin();
	//
	//
	// public void deletePost(Integer postId);
	//
	// public List<PostModel> getAllPostsPopularOrder();
	//
	// public void updateCount(int postId);
	//
	// public List<PostModel> getAllPostsHotest();
	//
	// public List<PostModel> getAllPostByCategoryId(HashMap<String, Integer>
	// mapParam);
	//
	// public List<PostModel> getAllPostByTagId(HashMap<String, Integer>
	// mapParam);
	//
	// public int getNumPost();
}

package vn.com.arillance.imagehub.config.app;

import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import vn.com.arillance.imagehub.controller.JobFinderController;
import vn.com.arillance.imagehub.exception.ExceptionMapper;

@Configuration
@ComponentScan("vn.com.arillance.imagehub")
public class JerseyConfig extends ResourceConfig {
	public JerseyConfig() {
		register(JobFinderController.class);
		register(ExceptionMapper.class);
		register(MultiPartFeature.class);

	}
}

package vn.com.arillance.imagehub.dao;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.session.SqlSession;

import vn.com.arillance.imagehub.mapper.CategoryMapper;
import vn.com.arillance.imagehub.model.CategoryModel;

public class CategoryDao {

	public List<CategoryModel> getListCategory(boolean incDeleted) {

		HashMap<String, Object> mapParam = new HashMap<String, Object>();
		mapParam.put("incDeleted", incDeleted);
		SqlSession sqlSession = MyBatisUtils.getSqlSessionFactory()
				.openSession();
		try {
			CategoryMapper CategoryMapper = sqlSession
					.getMapper(CategoryMapper.class);
			return CategoryMapper.getAllCategorys(mapParam);
		} finally {
			sqlSession.close();
		}

	}

}

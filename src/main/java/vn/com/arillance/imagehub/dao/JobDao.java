package vn.com.arillance.imagehub.dao;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Component;

import vn.com.arillance.imagehub.exception.ErrorMessage;
import vn.com.arillance.imagehub.mapper.JobMapper;
import vn.com.arillance.imagehub.mapper.UserMapper;
import vn.com.arillance.imagehub.model.JobModel;
import vn.com.arillance.imagehub.model.JobTitle;
import vn.com.arillance.imagehub.model.SearchJobModel;
import vn.com.arillance.imagehub.model.SearchModel;
import vn.com.arillance.imagehub.model.SkillSet;
import vn.com.arillance.imagehub.model.UserModel;
import vn.com.arillance.imagehub.utils.HTTPUtils;

@Component
public class JobDao {
	public ArrayList<JobModel> searchJob(SearchJobModel search) {
		SqlSession session = MyBatisUtils.getSqlSessionFactory().openSession();
		JobMapper mapper = session.getMapper(JobMapper.class);
		ArrayList<JobModel> listJob = mapper.searchJob(search);
		session.close();
		return listJob;
	}

	public JobModel getJob(String jobId) {
		SqlSession session = MyBatisUtils.getSqlSessionFactory().openSession();
		JobMapper mapper = session.getMapper(JobMapper.class);
		JobModel job = mapper.getJob(jobId);
		session.close();
		return job;
	}

	public int getMaxIdDb() {
		SqlSession session = MyBatisUtils.getSqlSessionFactory().openSession();
		JobMapper mapper = session.getMapper(JobMapper.class);
		int id = mapper.getMaxIdDb();
		session.close();
		return id;
	}

	public void createJob(JobModel job) {
		SqlSession session = MyBatisUtils.getSqlSessionFactory().openSession();
		JobMapper mapper = session.getMapper(JobMapper.class);
		if (job.getSalaryUnit() != null){
			job.setSalaryUnit(job.getSalaryUnit().toLowerCase());
		}
		mapper.createJob(job);
		mapper.createJobSkill(job);

		UserDao userDao = new UserDao();
		for (int skillId : job.getSkills()) {
			String notiToken = userDao.getNotiToken(skillId);
			HTTPUtils.pushNotficationToAllDevices(notiToken, "New Job Available", "A new job you might be interested!",
					HTTPUtils.NEW_ORDER);
		}
		session.commit();

		session.close();
	}

	public ArrayList<SkillSet> getSkillSet() {
		SqlSession session = MyBatisUtils.getSqlSessionFactory().openSession();
		JobMapper mapper = session.getMapper(JobMapper.class);
		ArrayList<SkillSet> skills = mapper.getSkillSet();
		session.close();
		return skills;
	}

	public ArrayList<String> getListCity() {
		SqlSession session = MyBatisUtils.getSqlSessionFactory().openSession();
		JobMapper mapper = session.getMapper(JobMapper.class);
		ArrayList<String> city = mapper.getListCity();
		session.close();
		return city;
	}

	public void addTitle(ArrayList<String> listArray) {
		SqlSession session = MyBatisUtils.getSqlSessionFactory().openSession();
		JobMapper mapper = session.getMapper(JobMapper.class);
		mapper.addTitle(listArray);
		session.commit();

		session.close();
	}

	public ArrayList<JobTitle> getTitleJobs() {
		SqlSession session = MyBatisUtils.getSqlSessionFactory().openSession();
		JobMapper mapper = session.getMapper(JobMapper.class);
		ArrayList<JobTitle> jobs = mapper.getListJob();

		session.close();
		return jobs;
	}

	public ArrayList<JobModel> getListJob(String userId) {
		SqlSession session = MyBatisUtils.getSqlSessionFactory().openSession();
		JobMapper mapper = session.getMapper(JobMapper.class);
		ArrayList<JobModel> jobs = mapper.getListJobByUser(userId);
		
		session.close();
		return jobs;
	}

	public int updateJobFlag(SearchModel user) {
		SqlSession session = MyBatisUtils.getSqlSessionFactory().openSession();
		UserMapper mapper = session.getMapper(UserMapper.class);
		HashMap<String, String> mapParam = new HashMap<String, String>();
		mapParam.put("username", user.getUserId());
		mapParam.put("password", user.getPassword());
		UserModel userModel = mapper.login(mapParam);
		if (userModel != null) {
			JobMapper jobmapper = session.getMapper(JobMapper.class);

			jobmapper.updateFlag(user);
			session.commit();
			session.close();
			return 1;
		} else {
			return 0;
		}

	}

	public ArrayList<JobModel> getHistoryJob(String userid) {
		SqlSession session = MyBatisUtils.getSqlSessionFactory().openSession();
		JobMapper mapper = session.getMapper(JobMapper.class);
		ArrayList<JobModel> jobs = mapper.getHistoryJob(userid);
		return jobs;
	}
	
	public ErrorMessage removeJob(JobModel job){
		SqlSession session = MyBatisUtils.getSqlSessionFactory().openSession();
		JobMapper mapper = session.getMapper(JobMapper.class);
		int update = mapper.removeJob(job);
		session.commit();
		if (update > 0){
			return new ErrorMessage(1, "success");
		} else{
			return new ErrorMessage(0, "No row removed!");
		}
	}
}

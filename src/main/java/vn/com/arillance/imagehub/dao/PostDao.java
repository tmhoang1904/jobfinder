package vn.com.arillance.imagehub.dao;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.session.SqlSession;

import vn.com.arillance.imagehub.mapper.PostMapper;
import vn.com.arillance.imagehub.model.PostModel;

public class PostDao {
	public List<PostModel> getListPost(String title, boolean incDeleted,
			boolean incDisable, int categoryId, Date startDate, Date endDate,
			int authorId, int iOrder, int page, int offset) {
		String order = "";
		switch (iOrder) {
		case 0:
			order = "posts.post_id";
			break;
		case 1:
			order = "posts.post_title";
			break;
		case 2:
			order = "posts.post_author_id";
			break;
		case 3:
			order = "posts.post_category_id";
			break;
		case 4:
			order = "posts.create_date";
			break;
		case 5:
			order = "posts.like_count";
			break;
		case 6:
			order = "posts.view_count";
			break;
		case 7:
			order = "(posts.like_count+posts.view_count)";
			Calendar c= Calendar.getInstance();
			c.add(Calendar.DATE, -3);
			startDate = c.getTime();
			break;
		default:
			order = "posts.create_date";
			break;
		}
		// title = title!=null?"'%"+title+"%'":title;
		HashMap<String, Object> mapParam = new HashMap<String, Object>();
		mapParam.put("title", title);
		mapParam.put("incDeleted", incDeleted);
		mapParam.put("incDisable", incDisable);
		mapParam.put("categoryId", categoryId);
		mapParam.put("startDate", startDate);
		mapParam.put("endDate", endDate);
		mapParam.put("authorId", authorId);
		mapParam.put("orderb", order);
		mapParam.put("orderb", order);
		mapParam.put("page", (page - 1) * offset);
		mapParam.put("offset", offset);
		SqlSession sqlSession = MyBatisUtils.getSqlSessionFactory()
				.openSession();
		try {
			PostMapper postMapper = sqlSession.getMapper(PostMapper.class);
			return postMapper.getAllPosts(mapParam);
		} finally {
			sqlSession.close();
		}
	}

	public PostModel getPost(int id) {
		SqlSession sqlSession = MyBatisUtils.getSqlSessionFactory()
				.openSession();
		try {
			PostMapper postMapper = sqlSession.getMapper(PostMapper.class);
			return postMapper.getPostById(id);
		} finally {
			sqlSession.close();
		}
	}

	public void addCount(int id) {
		SqlSession sqlSession = MyBatisUtils.getSqlSessionFactory()
				.openSession();
		try {
			PostMapper postMapper = sqlSession.getMapper(PostMapper.class);
			postMapper.updateCount(id);
			sqlSession.commit();
		} finally {
			sqlSession.close();
		}
	}

	public void insertPost(PostModel post) {
		SqlSession sqlSession = MyBatisUtils.getSqlSessionFactory()
				.openSession();
		try {
			PostMapper postMapper = sqlSession.getMapper(PostMapper.class);
			postMapper.insertPost(post);
			sqlSession.commit();
		} finally {
			sqlSession.close();
		}
	}

	public void update(PostModel post) {
		SqlSession sqlSession = MyBatisUtils.getSqlSessionFactory()
				.openSession();
		try {
			PostMapper postMapper = sqlSession.getMapper(PostMapper.class);
			postMapper.updatePost(post);
			sqlSession.commit();
		} finally {
			sqlSession.close();
		}
	}

	public void delete(int id) {
		SqlSession sqlSession = MyBatisUtils.getSqlSessionFactory()
				.openSession();
		try {
			PostMapper postMapper = sqlSession.getMapper(PostMapper.class);
			postMapper.deletePost(id);
			sqlSession.commit();
		} finally {
			sqlSession.close();
		}
	}

	public void enable(int id) {
		SqlSession sqlSession = MyBatisUtils.getSqlSessionFactory()
				.openSession();
		try {
			PostMapper postMapper = sqlSession.getMapper(PostMapper.class);
			postMapper.enablePost(id);
			sqlSession.commit();
		} finally {
			sqlSession.close();
		}
	}
}

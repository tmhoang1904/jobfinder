package vn.com.arillance.imagehub.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Component;

import vn.com.arillance.imagehub.exception.ErrorMessage;
import vn.com.arillance.imagehub.mapper.UserMapper;
import vn.com.arillance.imagehub.model.ProfileModel;
import vn.com.arillance.imagehub.model.SearchModel;
import vn.com.arillance.imagehub.model.SkillSet;
import vn.com.arillance.imagehub.model.UserModel;

@Component
public class UserDao {
	public UserModel login(String username, String password) {
		SqlSession session = MyBatisUtils.getSqlSessionFactory().openSession();
		UserMapper mapper = session.getMapper(UserMapper.class);
		HashMap<String, String> mapParam = new HashMap<String, String>();
		mapParam.put("username", username);
		mapParam.put("password", password);

		UserModel user = mapper.login(mapParam);
		session.close();
		return user;
	}

	public void updateAccount(UserModel user) {
		SqlSession session = MyBatisUtils.getSqlSessionFactory().openSession();
		UserMapper mapper = session.getMapper(UserMapper.class);
		mapper.updateAccount(user);
		session.commit();

		session.close();
	}

	public void changePassword(UserModel user) {
		SqlSession session = MyBatisUtils.getSqlSessionFactory().openSession();
		UserMapper mapper = session.getMapper(UserMapper.class);
		mapper.changePassword(user);
		session.commit();
		session.close();
	}

	public void register(UserModel user) {
		SqlSession session = MyBatisUtils.getSqlSessionFactory().openSession();
		UserMapper mapper = session.getMapper(UserMapper.class);
		mapper.register(user);
		// mapper.createUserSkill(user);
		session.commit();
		session.close();
	}

	public void createProfile(ProfileModel profile) {
		SqlSession session = MyBatisUtils.getSqlSessionFactory().openSession();
		UserMapper mapper = session.getMapper(UserMapper.class);
		mapper.createProfile(profile);
		mapper.createUserSkill(profile);
		session.commit();
		session.close();
	}

	public ArrayList<ProfileModel> search(SearchModel search) {
		SqlSession session = MyBatisUtils.getSqlSessionFactory().openSession();
		UserMapper mapper = session.getMapper(UserMapper.class);
		ArrayList<ProfileModel> listUser = mapper.searchUser(search);
		session.close();
		return listUser;
	}

	public ProfileModel getuser(String userId) {
		SqlSession session = MyBatisUtils.getSqlSessionFactory().openSession();
		UserMapper mapper = session.getMapper(UserMapper.class);
		ProfileModel user = mapper.getuser(userId);
		session.close();
		return user;
	}

	public int updatePosition(SearchModel user) {
		SqlSession session = MyBatisUtils.getSqlSessionFactory().openSession();
		UserMapper mapper = session.getMapper(UserMapper.class);
		HashMap<String, String> mapParam = new HashMap<String, String>();
		mapParam.put("username", user.getUserId());
		mapParam.put("password", user.getPassword());
		UserModel userModel = mapper.login(mapParam);
		if (userModel != null) {
			mapper.updatePosition(user);
			session.commit();
			session.close();
			return 1;
		} else {
			return 0;
		}
	}

	public boolean checkEmail(String email) {
		SqlSession session = MyBatisUtils.getSqlSessionFactory().openSession();
		UserMapper mapper = session.getMapper(UserMapper.class);
		String rtn = mapper.checkEmail(email);
		if (rtn != null) {
			return true;
		}
		session.close();
		return false;
	}

	public ArrayList<ProfileModel> getListProfile(String userId) {
		SqlSession session = MyBatisUtils.getSqlSessionFactory().openSession();
		UserMapper mapper = session.getMapper(UserMapper.class);
		ArrayList<ProfileModel> listProfile = mapper.getListProfile(userId);
		session.close();
		return listProfile;

	}

	public int updateFlag(SearchModel user) {
		SqlSession session = MyBatisUtils.getSqlSessionFactory().openSession();
		UserMapper mapper = session.getMapper(UserMapper.class);
		HashMap<String, String> mapParam = new HashMap<String, String>();
		mapParam.put("username", user.getUserId());
		mapParam.put("password", user.getPassword());
		UserModel userModel = mapper.login(mapParam);
		if (userModel != null) {
			mapper.updateFlag(user);
			session.commit();
			session.close();
			return 1;
		} else {
			return 0;
		}
	}

	public UserModel loginSocial(UserModel user) {
		SqlSession session = MyBatisUtils.getSqlSessionFactory().openSession();
		UserMapper mapper = session.getMapper(UserMapper.class);

		if (!mapper.getLoginSocial(user.getUserId())) {
			mapper.register(new UserModel(user.getUserId(), user.getName(), UUID.randomUUID().toString(), true,
					user.getImgUrl(), "", false, "", "", "", "", ""));
			session.commit();
		}
		UserModel userSocial = mapper.loginSocial(user.getUserId());
		session.close();
		return userSocial;
	}
	
	public String getNotiToken(int skillId) {
		SqlSession session = MyBatisUtils.getSqlSessionFactory().openSession();
		UserMapper mapper = session.getMapper(UserMapper.class);
		String token = mapper.getNotiToken(skillId);
		session.close();
		return token;
	}

	public List<String> getNotiTokenFromJob(String jobId) {
		SqlSession session = MyBatisUtils.getSqlSessionFactory().openSession();
		UserMapper mapper = session.getMapper(UserMapper.class);
		List<String> token = mapper.getNotiTokenFromJob(jobId);
		session.close();
		return token;
	}

	public List<SkillSet> getAllSkillsEmp(String profileId) {
		SqlSession session = MyBatisUtils.getSqlSessionFactory().openSession();
		UserMapper mapper = session.getMapper(UserMapper.class);
		List<SkillSet> skillSets = mapper.getAllSkillsEmp(profileId);
		session.close();
		return skillSets;
	}

	public ErrorMessage updateNotiToken(String profileId, String notiToken) {
		SqlSession session = MyBatisUtils.getSqlSessionFactory().openSession();
		UserMapper mapper = session.getMapper(UserMapper.class);

		List<SkillSet> skillSets = mapper.getAllSkillsEmp(profileId);
		for (SkillSet skill : skillSets) {
			HashMap<String, Object> mapParam = new HashMap<String, Object>();
			mapParam.put("skillId", skill.getSkillId());
			// mapParam.put("notiToken", notiToken);
			// Check if not contain token, then append it with seperator is ","
			String curToken = mapper.getNotiToken(skill.getSkillId());
			if (curToken == null || !curToken.contains(notiToken)) {
				if (curToken != null) {
					curToken += "," + notiToken;
				} else {
					curToken = notiToken;
				}
				mapParam.put("notiToken", curToken);
				int update = mapper.updateNotiToken(mapParam);
				session.commit();
				if (update > 0) {
					session.close();
					return new ErrorMessage(1, "sucess");
				} else {
					session.close();
					return new ErrorMessage(0, "no updated row");
				}
			} else {
				session.close();
				return new ErrorMessage(1, "no token need to update");
			}
		}
		session.close();
		return new ErrorMessage(0, "No updated row");
	}
	
	public ErrorMessage removeProfile(String profileId){
		SqlSession session = MyBatisUtils.getSqlSessionFactory().openSession();
		UserMapper mapper = session.getMapper(UserMapper.class);		
		int update = mapper.removeProfile(profileId);
		session.commit();
		if (update > 0){
			return new ErrorMessage(1, "success");
		} else{
			return new ErrorMessage(0, "no row updated!");
		}
	}
}
